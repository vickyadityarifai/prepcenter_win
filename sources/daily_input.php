<?php 

if(!isset($_SESSION)) 
{ 
    session_start(); 
}
// do check
if (!isset($_SESSION["username"])) {
    header("location: ../login.php");
    exit; // prevent further execution, should there be more code that follows
}

//include 'C:\inetpub\wwwroot\prepcenter\sources\conn.php';
include 'conn.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="../image/png" href="../assets/img/favicon.png">
  <title>
    Prepcenter
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
</head>
<body class="g-sidenav-show  bg-gray-200">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="index.php">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Prepcenter</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white" href="index.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li>
          <a class="nav-link text-white active bg-gradient-info" href="daily_input.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">add_task</i>
            </div>
            <span class="nav-link-text ms-1">Daily Input</span>
          </a>          
        </li>        
        <?php
          if ($_SESSION['role_id'] == '1'){
              echo "
              <li class='nav-item'>
                <a class='nav-link text-white' href='employee.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>table_view</i>
                  </div>
                  <span class='nav-link-text ms-1'>Employee</span>
                </a>
              </li> 
              <li class='nav-item'>
                <a class='nav-link text-white' href='products.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>inventory</i>
                  </div>
                  <span class='nav-link-text ms-1'>Products</span>
                </a>
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Report pages</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Employee</span>
                </a> 
              </li>
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage_time.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Time</span>
                </a> 
              </li>                
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Summary</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='monthly_date_summary.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>app_registration</i>
                  </div>
                  <span class='nav-link-text ms-1'>Mothly Date Summary</span>
                </a> 
              </li>
              ";
          } else {
              echo "";
          }
        ?>
<!--         <li class="nav-item">
          <a class="nav-link text-white" href="report.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report By Employee</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="report_old.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report By Time</span>
          </a>
        </li> -->
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-info mt-4 w-100" href="logout.php" type="button">Log Out</a>
      </div>
    </div>    
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">

    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" data-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Daily Input</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Daily Input</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="#" class="nav-link text-body font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">
                  <?php echo $_SESSION['username']; ?>
                </span>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-right-from-bracket cursor-pointer"></i>
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="logout.php">
                    <div class="d-flex py-1">
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span>Log Out Now</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          <div id="current_date"></p>
                          <script>
                          document.getElementById("current_date").innerHTML = Date();
                          </script>                          
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

<div class="container-fluid py-4">

<?php 
  if(isset($_GET['alert'])){
    if($_GET['alert'] == "success"){
      echo "<div class='alert alert-success alert-dismissible text-white fade show my-4' role='alert'>
              <span><strong>Success!</strong> Data has been saved !!</span>
              <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>";
    } elseif ($_GET['alert'] == "successdelete") {
      echo "<div class='alert alert-success alert-dismissible text-white fade show my-4' role='alert'>
              <span><strong>Success!</strong> Data has been delete !!</span>
              <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>";
    } else {
      echo "<div class='alert alert-danger alert-dismissible text-white fade show my-4' role='alert'>
            <span><strong>Failed!</strong> Data has not saved !!</span>
              <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>            
            </div>";
    }
  }
?>

<!---CUSTOME DATE SEARCH--->

<form method="get">
  <div class="row">
    <div class="col-md-4">
      <div class="input-group input-group-static my-3">
        <label>Select Date Range :</label>
          <?php
          $date_range = date('Y-m-d').'_'.date('Y-m-d');
          if(isset($_GET['date_range'])){
              $date_range = $_GET['date_range'];
          }
          ?>
          <input type="hidden" name="date_range" id="date_range" value="<?php echo $date_range; ?>">          
          <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
              <i class="fa fa-calendar"></i>&nbsp;
              <span><?php echo $date_range; ?></span> <i class="fa fa-caret-down"></i>
          </div>
      </div>
    </div>

    <div class="col-md-2" style="height: 50px; width: 104px;">
      <div class="input-group input-group-static my-4">
        <button type="submit" name="submit" class="btn bg-gradient-info">Search</button>
      </div>
    </div>
    <div class="col-md-2">
      <div class="input-group input-group-static my-4">
        <button type="reset" name="reset" class="btn bg-gradient-danger"><a href="daily_input.php">Clear</a></button>
      </div>
    </div>
  </div>
</form>    

<!---END CUSTOME DATE SEARCH--->

<div class="row">
<div class="col-12">
  <div class="card my-4">
    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
      <div class="bg-gradient-info shadow-info border-radius-lg pt-4 pb-3">
        <div><h6 class="text-white text-capitalize ps-3" align="left">Daily Input Record | <a href="daily_input_add.php"><i class="material-icons text-sm">add</i>Add Daily Input</a></h6></div>
      </div>
    </div>

    <?php
    $columns = array('date');
    $column = isset($_GET['column']) && in_array($_GET['column'], $columns) ? $_GET['column'] : $columns[0];
    $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'desc' ? 'DESC' : 'ASC';
    $up_or_down = str_replace(array('ASC','DESC'), array('up','down'), $sort_order); 
    $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';
    $add_class = ' class="highlight"';
    ?>

      <table class="table align-items-center mb-0" id="myTable">
        <thead>
          <tr>
            <th></th>
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"><a href="daily_input.php?date_range=<?php echo $date_range; ?>&column=date&order=<?php echo $asc_or_desc; ?>&submit=">Working Date<i class="fas fa-sort<?php echo $column == 'date' ? '-' . $up_or_down : ''; ?>"></i></a></th>            
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Employee Name</th>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">Start Time</th>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">End Time</th>
            <?php 
              if ($_SESSION['role_id'] == '1'){
                echo "<th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1'>Rate / Hour</th>";
              } else {
                echo "";
              }
            ?>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">Total Time</th>
            <?php 
              if ($_SESSION['role_id'] == '1'){
                echo "<th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1'>Total Paid</th>";
              } else {
                echo "";
              }
            ?>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">QTY</th>
            <?php 
              if ($_SESSION['role_id'] == '1'){
                echo "<th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1'>PC / Item</th>
                      <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1'>Item / Hour</th>";
              } else {
                echo "";
              }
            ?>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Details</th>
            <?php
              if ($_SESSION['role_id'] == '1'){
                echo "<th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2'>Delete</th>";
              } else {
                echo "";
              }
            ?>
          </tr>
        </thead>
      <?php

      include 'conn.php';
      $no=1;

      $daterange = '';
      if(!empty($date_range)){
          $date_range = explode ("_", $date_range);

          $date_start = strtotime($date_range[0]);
          $mil = $date_start;
          $seconds = ceil($date_start / 1000);
          $date_start_conv = date("Y-m-d", $date_start);

          $date_end = strtotime($date_range[1]);
          $mil = $date_end;
          $seconds = ceil($date_end / 1000);
          $date_end_conv = date("Y-m-d", $date_end);
      }          

      // $data = mysqli_query($koneksi,"
      //   SELECT employee.first_name, employee.last_name, employee.rate, daily_input.id, daily_input.date, daily_input.start_time, daily_input.endtime, daily_input.total_time_in_sec, daily_input.total_paid, daily_input.total_packing_cost, daily_input.total_item_hour
      //   FROM employee
      //   INNER JOIN daily_input ON employee.id = daily_input.id_employee
      //   ORDER BY daily_input.id DESC
      //   ");

      // if ($result = $mysqli->query('SELECT * FROM daily_input ORDER BY ' .  $column . ' ' . $sort_order)) {
      //   // Some variables we need for the table.

      if(isset($_GET['submit'])){
        $columns = array('date');
        $column = isset($_GET['column']) && in_array($_GET['column'], $columns) ? $_GET['column'] : $columns[0];        
        $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'desc' ? 'DESC' : 'ASC';
        $up_or_down = str_replace(array('ASC','DESC'), array('up','down'), $sort_order); 
        $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';
        $daterange = "WHERE daily_input.date BETWEEN '" . $date_start_conv . "' AND '" . $date_end_conv . "'";
        $sql = mysqli_query($koneksi,"
          SELECT 
            employee.first_name, 
            employee.last_name, 
            employee.rate, 
            daily_input.id, 
            daily_input.date, 
            daily_input.start_time, 
            daily_input.endtime, 
            daily_input.total_time_in_sec, 
            daily_input.total_paid, 
            daily_input.total_packing_cost, 
            daily_input.total_item_hour
          FROM employee
          INNER JOIN daily_input ON employee.id = daily_input.id_employee
          $daterange
          ORDER BY daily_input.date " .$sort_order
        );
      } else {
        $sql = mysqli_query($koneksi,"
          SELECT 
            employee.first_name, 
            employee.last_name, 
            employee.rate, 
            daily_input.id, 
            daily_input.date, 
            daily_input.start_time, 
            daily_input.endtime, 
            daily_input.total_time_in_sec, 
            daily_input.total_paid, 
            daily_input.total_packing_cost, 
            daily_input.total_item_hour
          FROM employee
          INNER JOIN daily_input ON employee.id = daily_input.id_employee
          WHERE daily_input.date > NOW() - INTERVAL 60 DAY AND daily_input.date < NOW() + INTERVAL 60 DAY
          ORDER BY daily_input.date DESC
        ");
      }          

      while($d = mysqli_fetch_array($sql)){
        $id = $d['id'];
        $date = $d['date'];
        $employee_name = $d['last_name']." ".$d['first_name'];
        $start_time = $d['start_time'];
        $endtime = $d['endtime'];
        $rate = $d['rate'];
        $total_time_in_sec = $d['total_time_in_sec'];
        $total_paid = $d['total_paid'];
        $total_packing_cost = $d['total_packing_cost'];
        $total_item_hour = $d['total_item_hour'];

      ?>

        <tbody>
        <tr>
          <td>
            <!-- <p class="text-xs font-weight-normal mb-0 ps-3"><?php echo $no++; ?></p> -->
          </td>          
          <td <?php echo $column == 'date' ? $add_class : ''; ?>>
            <div class="d-flex align-items-center">
              <p class="text-xs font-weight-normal mb-0"><?php $day = date('D', strtotime($date)); echo $day.", ".date('F d, Y', strtotime($date)); ?></p>
            </div>
          </td>
          <td>
            <div class="d-flex">
              <div class="my-auto">
                <h6 class="mb-0 text-xs"><?php echo $employee_name; ?></h6>
              </div>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-1"><?php echo $start_time; ?></p>
              </span>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-1"><?php echo $endtime; ?></p>
              </span>
            </div>
          </td>
          <?php
            if ($_SESSION['role_id'] == '1'){ 
              echo "
              <td>
                <div class='d-flex align-items-center'>
                  <span class='text-dark text-xs'>
                    <p class='text-xs font-weight-normal mb-0 ps-4'>";
              
              echo "$".$rate;

              echo "</p>
                  </span>
                </div>
              </td>";
            } else {
              echo "";
            }
          ?>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-2">
                  <?php 
                    $time = $total_time_in_sec; 
                    // $hour = round(($time / 3600)); //hour
                    // $minute = round(($time / 60) % 60); //minute                
                    
                    // if (($time / 3600 %2) == 0){
                    //   if ($time > 60 AND $time < 3600) {
                    //     echo $timeex = "0 H ".($minute % 60)." m";
                    //   } else if ($time > 3600 AND $time < 24*60*60) {
                    //     echo $timeex = (($hour % 24) - 1)." H ".($minute % 60)." m";
                    //   }                    
                    // } else {
                    //   if ($time > 60 AND $time < 3600) {
                    //     echo $timeex = "0 H ".($minute % 60)." m";
                    //   } else if ($time > 3600 AND $time < 24*60*60) {
                    //     echo $timeex = ($hour % 24)." H ".($minute % 60)." m";
                    //   }                                          
                    // }

                    $hours = $time / 60;
                    $mins = $hours % 60;
                    $hours = $hours / 60;

                    echo $timeex = ((int)$hours . " H " . (int)$mins . " m"); //ultimate time display
                  
                  ?>
                </p>
              </span>
            </div>
          </td>          
          <?php
            if ($_SESSION['role_id'] == '1'){ 
              echo "
              <td>
                <div class='d-flex align-items-center'>
                  <span class='text-dark text-xs'>
                    <p class='text-xs font-weight-normal mb-0 ps-2'>";
              
              echo "$".$total_paid;

              echo "</p>
                  </span>
                </div>
              </td>";
            } else {
              echo "";
            }
          ?>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-1">
                  <?php 
                    $qty_display = 0;
                    // $qty_old = "SELECT daily_input.date, daily_input_detail.fnsku, import_result.title, daily_input_detail.pcs, daily_input_detail.qty, daily_input.total_packing_cost
                    // FROM daily_input_detail
                    // INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
                    // INNER JOIN import_result ON import_result.fnsku = daily_input_detail.fnsku
                    // WHERE daily_input.id = '".$d['id']."'
                    // GROUP BY daily_input.date ";
                    $qty = 
                        "
                        SELECT SUM(daily_input_detail.qty) AS total_qty
                        FROM daily_input_detail
                        INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
                        WHERE daily_input_detail.id_daily_input = '".$id."' 
                        ";
                    $queryqty = mysqli_query($koneksi, $qty);
                    while ($qtyshow = mysqli_fetch_array($queryqty)) {
                      //$qty_display = $qtyshow['qty'];
                      $qty_display = $qtyshow['total_qty'];
                    }
                    
                    if ($qty_display > 0) {
                      echo $qty_display;
                    } else {
                      echo "<b style='color:red;'>0</b>";
                    }
                  ?>
                </p>
              </span>
            </div>
          </td>
          <?php
            if ($_SESSION['role_id'] == '1'){ 
              echo "
              <td>
                <div class='d-flex'>
                  <div class='my-auto'>
                    <h6 class='mb-0 text-xs ps-2'>";
              echo "$".$total_packing_cost;
              echo "</h6>
                  </div>
                </div>
              </td>
              <td>
                <div class='d-flex'>
                  <div class='my-auto'>
                    <h6 class='mb-0 text-xs ps-2'>";
              echo $total_item_hour;
              echo "</h6>
                  </div>
                </div>
              </td>";
            } else {
              echo "";
            }
          ?>
          <td class="align-middle">
            <div>
              <h6 class="text-white text-capitalize ps-2 my-1" align="left">
                <a href="daily_input_detail.php?id=<?php echo $id; ?>"><i class="material-icons text-sm">settings</i>Detail
                </a>
                <!-- <a href="daily_input_detail_prototype.php?id=<?php echo $d['id']; ?>"><i class="material-icons text-sm">settings</i>Detail
                </a> -->
              </h6>
            </div>
          </td>
          <?php
            if ($_SESSION['role_id'] == '1'){
              echo "
              <td class='align-middle'>
                <div>
                  <h6 class='text-white text-capitalize ps-2 my-1 remove' align='left'>";
              echo "<a href='javascript:deleteData(".$id.")'><i class='material-icons text-sm'>delete</i>Delete</a>";
              echo "</h6>
                </div>
              </td>";
            } else {
              echo "";
            }
          ?>
        </tr>
        </tbody>

      <?php 
      }
      ?>            
      </table>
        <i style="text-align: center;">*Please, Select Date Range First.</i>
		  </div>
	  </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

<!-- <script type="text/javascript">
    $(".remove").click(function(){
      var id = $(this).parents("tr").attr("id");

      if(confirm('Are you sure to remove this record ?'))
      {
        $.ajax({
           url: 'daily_input_delete.php',
           type: 'GET',
           data: {id: id},
           error: function() {
              alert('Something is wrong');
           },
           success: function(data) {
              $("#"+id).remove();
              alert('Record removed successfully');  
           }
        });
      }
      return false;
    });
</script> -->
<script type="text/javascript">
  function deleteData(id){
    if (confirm("Do You want to Delete this Record ?")){
      window.location.href = 'daily_input_delete.php?id=' + id;
    }
  }
</script>

<!--- CUSTOME DATE SEARCH --->

<!--   Core JS Files   -->
<script src="../assets/js/core/popper.min.js" ></script>
<script src="../assets/js/core/bootstrap.min.js" ></script>
<script src="../assets/js/plugins/perfect-scrollbar.min.js" ></script>
<script src="../assets/js/plugins/smooth-scrollbar.min.js" ></script>

<script>
  var win = navigator.platform.indexOf('Win') > -1;
  if (win && document.querySelector('#sidenav-scrollbar')) {
    var options = {
      damping: '0.5'
    }
    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
  }
</script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=3.0.4"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
$(function() {

    <?php
    if(isset($_GET['date_range'])){
      $date_range = explode ("_", $_GET['date_range']); 
    ?>
      var start = moment(new Date('<?php echo $date_range[0]; ?>'));
      // var start = moment();//moment().subtract(29, 'days');
      var end = moment(new Date('<?php echo $date_range[1]; ?>'));
      // var end = moment();

      // var start = moment(new Date('<?php echo $date_start_conv; ?>'));
      // // var start = moment();//moment().subtract(29, 'days');
      // var end = moment(new Date('<?php echo $date_end_conv; ?>'));
      // // var end = moment();

    <?php
    } else {
    ?>
      var start = moment();//moment().subtract(29, 'days');
      var end = moment();
    <?php
    }
    ?>

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $("#date_range").val(start.format('YYYY-M-D') + '_' + end.format('YYYY-M-D'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Last 3 Months': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')],
           'Last 6 Months': [moment().subtract(5, 'month').startOf('month'), moment().endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
           'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

    cb(start, end);

});
</script>
<!--- END CUSTOME DATE SEARCH --->

<?php include '../pages/footer.html'; ?>