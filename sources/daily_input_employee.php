<?php

session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: ../login.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

$id = $_REQUEST['id'];
$date = $_REQUEST['date_production'];
$starttime = $_REQUEST['start_time_production'];
$endtime = $_REQUEST['end_time_production'];

$getrate = mysqli_query($koneksi, "SELECT * FROM employee WHERE id = '$id' ");
while ($a = mysqli_fetch_array($getrate)){

// echo "ID : ".$id."<br />
//    date : ".$date."<br />
//    start time : ".$starttime."<br />
//    Endtime : ".$endtime."<br />";

$init = (strtotime($endtime) - strtotime($starttime));
$hour = round(($init / 3600)); //hour
$minute = round(($init / 60) % 60); //minute                
if ($init > 60 AND $init < 3600) {
  echo $time = "0 H ".($minute % 60)." m";
} else if ($init > 3600 AND $init < 24*60*60) {
  echo $time = ($hour % 24)." H ".($minute % 60)." m";
}

// echo "<br/ >Init mentah : ".$init.
//     "<br />Paid : $".
    $total_paid = round(($init / 3600) * $a['rate'], 2);

}

$paid = $total_paid;
$total_packing_cost = 0;

$query = "INSERT INTO daily_input VALUES(null,'$id','$date','$starttime','$endtime','$init','$paid','$total_packing_cost')";

$sql = mysqli_query($koneksi, $query); 

////////////////////////////////////////detail daily input field////////////////////////////////////////

$id_daily_input = $_REQUEST['id'];
$date = $_REQUEST['date_production'];
$fnsku = substr($_REQUEST['fnsku'], 0,10);
$input_to_fnsku = $fnsku;
$pack = $_REQUEST['pack'];
$qtyprod = $_REQUEST['qty'];

// echo "<br>////////////////detail input////////<br>ID : ".$id_daily_input."<br />
//    date : ".$date."<br />
//    fnsku : ".$input_to_fnsku."<br />
//    pack : ".$pack."<br />
//    qty input daily detail : ".$qtyprod."<br />";

$query1 = "INSERT INTO daily_input_detail VALUES(null,'$id_daily_input','$input_to_fnsku','$pack','$qtyprod')";
mysqli_set_charset($koneksi, "utf8");
$sql1 = mysqli_query($koneksi, $query1); 

//query2 = Get total paid by working hour, get total qty per day by date to count total_packing_cost / day

// $get_total_paid = mysqli_query($koneksi, "SELECT total_paid FROM daily_input WHERE id = '$id_daily_input'");
// while($a = mysqli_fetch_array($get_total_paid)){
//     $total_paid_daily = $a['total_paid'];
// } //AWALNYA TOTAL PAID DAILY PAKAI QUERY INI, TAPI KARENA ADA QUERY COUNT DAN INPUT DARI QUERY $sql DIATAS, JADI TOTAL PAID DAILY, DIAMBIL BERDASARKAN PERHITUNGAN $paid DIATAS. UNTUK KEMUDIAN DIHITUNG UNTUK MENDAPATKAN TOTAL PACKING COST

$gettotalqty = mysqli_query($koneksi, "
  SELECT SUM(daily_input_detail.qty) AS total_qty
  FROM daily_input_detail
  INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
  WHERE daily_input_detail.id_daily_input = '$id_daily_input' AND daily_input.date = '$date'
  ");

while($totalqtydaily = mysqli_fetch_array($gettotalqty)){
  //echo "qty total by date and ID daily_input = ".$totalqtydaily['total_qty'];
  $totalqtydailytocount = $totalqtydaily['total_qty'];
}

$to_int = (int) $paid;
// echo "<br> total paid daily = ".$to_int."<br>";
// echo "<br> total QTY by ID daily input and DATE = ".$totalqtydailytocount."<br>";
$total_paid_to_insert = round($to_int/$totalqtydailytocount, 2);
// echo $total_paid_to_insert;

$query2 = "UPDATE daily_input SET total_packing_cost = '$total_paid_to_insert' WHERE id = '$id_daily_input' ";
$sql2 = mysqli_query($koneksi, $query2);

//query3 = Update PCS and QTY to table import_result
$sqlgetpcsandqty = mysqli_query($koneksi, "
  SELECT * FROM import_result
  WHERE fnsku = '$input_to_fnsku'
  ");
while ($updatepcsandqty = mysqli_fetch_array($sqlgetpcsandqty)) {
  $pcsawal = $updatepcsandqty['pcs'];
  $qtyawal = $updatepcsandqty['qty'];
}

$pcsakhir = $pcsawal + $pack;
// echo "PCS Awal = ". $pcsawal."<br>";
// echo "PCS Akhir = ". $pcsakhir."<br>";
$qtyakhir = $qtyawal + $qtyprod;
// echo "QTY Awal = ". $qtyawal."<br>";
// echo "QTY Akhir = ". $qtyakhir;

$query3 = "UPDATE `import_result` SET `pcs`='$pcsakhir',`qty`='$qtyakhir' WHERE fnsku = '$input_to_fnsku' ";
$sql3 = mysqli_query($koneksi, $query3);

if($sql && $sql1 && $sql2 && $sql3) {
    echo "<script type='text/javascript'>document.location.href = 'daily_input.php?alert=success';</script>";
} else {
    echo "<script type='text/javascript'>document.location.href = 'daily_input.php?alert=failed';</script>";
}

?>