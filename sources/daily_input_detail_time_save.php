<?php

if(!isset($_SESSION)) 
{ 
    session_start(); 
}
//session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: ../login.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

$id = $_REQUEST['id'];
$date = $_REQUEST['date'];
$starttime = $_REQUEST['start_time_production'];
$endtime = $_REQUEST['end_time_production'];

// echo "ID : ".$id."<br />
//       Date : ".$date."<br />
//       Start Time : ".$starttime."<br />
//       End Time : ".$endtime;                  //VALIDATION

//get employee rate and total paid
$getrate = mysqli_query($koneksi, "
  SELECT * FROM daily_input
  INNER JOIN employee ON daily_input.id_employee = employee.id
  WHERE daily_input.id = '$id'
  ");
while ($a = mysqli_fetch_array($getrate)){

  $init = (strtotime($endtime) - strtotime($starttime));
  $hour = round(($init / 3600)); //hour
  $minute = round(($init / 60) % 60); //minute                
  if ($init > 60 AND $init < 3600) {
    $time = "0 H ".($minute % 60)." m";
  } else if ($init > 3600 AND $init < 24*60*60) {
    $time = ($hour % 24)." H ".($minute % 60)." m";
  }
  // echo "<br/ >Init mentah : ".$init.
  //     "<br />Paid : $".
      $total_paid = round(($init / 3600) * $a['rate'], 2);
}

// echo "<br />
//       Time (HOUR) : ".($init / 3600)."<br />
//       1. Total Paid (Total Working Time Daily * Rate/Hour) : $".$total_paid;       //VALIDATION

//get qty daily
$sql = mysqli_query($koneksi, "
  SELECT SUM(daily_input_detail.qty) AS total_qty
  FROM daily_input_detail
  INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
  WHERE daily_input_detail.id_daily_input = '".$id."' AND daily_input.date = '".$date."'
  ");

while($totalqtydaily = mysqli_fetch_array($sql)){
//  echo "<br />Qty total by date and ID daily_input = ".$totalqtydaily['total_qty'];       //VALIDATION
  $totalqtydailytocount = $totalqtydaily['total_qty'];
}

if ($totalqtydailytocount > 0) {
  $total_packing_cost = round($total_paid/$totalqtydailytocount, 2);
  $total_item_hour = round($totalqtydailytocount / ($init / 3600), 2);
} else {
  $total_packing_cost = 0;
  $total_item_hour = 0;
}
// echo "<br />
//       2. Total packing cost (Total Paid Daily / Total QTY Daily) : ".$total_packing_cost."<br>
//       3. Total item hour (Total QTY Daily / Total Working Time Daily) : ".$total_item_hour;       //VALIDATION

$query = "UPDATE daily_input SET start_time = '$starttime', endtime = '$endtime', total_time_in_sec = '$init', total_paid = '$total_paid', total_packing_cost = '$total_packing_cost', total_item_hour = '$total_item_hour' WHERE id = '".$id."' ";
$sql = mysqli_query($koneksi, $query);

if($sql) {
    //echo "<script type='text/javascript'>document.location.href = 'daily_input_detail_update.php?id=$id';</script>";
    echo "<script type='text/javascript'>document.location.href = 'daily_input_detail.php?id=".$id."';</script>";
    //echo "<script>alert('QTY Has Been Update!!!');</script>";
} else {
    //echo "<script type='text/javascript'>document.location.href = 'daily_input_detail_update.php?id=$id';</script>";
    echo "<script type='text/javascript'>document.location.href = 'daily_input_detail.php?id=".$id."';</script>";
    //echo "<script>alert('QTY Failed To Update!!!');</script>";      
}

?>