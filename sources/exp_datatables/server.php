<?php
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'import_result';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'id', 'dt' => 0 ),
    array( 'db' => 'title', 'dt' => 1 ),
    array( 'db' => 'msku',  'dt' => 2 ),
    array( 'db' => 'asin',   'dt' => 3 ),
    array( 'db' => 'fnsku',   'dt' => 4 ),
    array( 'db' => 'pcs',   'dt' => 5 ),
    array( 'db' => 'qty',   'dt' => 6 )           
   
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'root',
    //'pass' => 'Mysql@1qaz2wsx3edc', //ubuntuserver
    'pass' => '',
    'db'   => 'inventorydb-3138355556',
    'host' => 'localhost'
);

//doroyal
// $sql_details = array(
//     'user' => 'avggyuve_prepcenter',
//     'pass' => 'db_avggyuve_prepcenter_pass',
//     'db'   => 'avggyuve_prepcenter',
//     'host' => 'localhost'
// );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'vendor/DataTables/server-side/scripts/ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);