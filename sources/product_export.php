<!DOCTYPE html>
<head>
    <title>Products Data of Prepcenter App</title>
    <link href="exp_datatables/style.css" rel="stylesheet" type="text/css" />

    <script src="exp_datatables/vendor/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
    <link rel="stylesheet"  href="exp_datatables/vendor/DataTables/jquery.datatables.min.css">	
    <script src="exp_datatables/vendor/DataTables/jquery.dataTables.min.js" type="text/javascript"></script> 

    <link rel="stylesheet"  href="exp_datatables/vendor/DataTables/buttons.datatables.min.css">    
    <script src="exp_datatables/vendor/DataTables/dataTables.buttons.min.js" type="text/javascript"></script> 
    <script src="exp_datatables/vendor/DataTables/jszip.min.js" type="text/javascript"></script> 
    <script src="exp_datatables/vendor/DataTables/pdfmake.min.js" type="text/javascript"></script> 
    <script src="exp_datatables/vendor/DataTables/vfs_fonts.js" type="text/javascript"></script> 
    <script src="exp_datatables/vendor/DataTables/buttons.html5.min.js" type="text/javascript"></script> 

    <script>
        $(document).ready(function () {
            var table = $('#productTable').DataTable({
                "paging": false,
                "processing": true,
                "serverSide": true,
                'serverMethod': 'post',
                "ajax": "exp_datatables/server.php",
                dom: 'Bfrtip',
                buttons: [
                    {extend: 'copy', attr: {id: 'allan'}}, 'csv', 'excel', 'pdf'
                ]
            });

        });
    </script>
    <style type="text/css">
        a {
          text-decoration: none;
          display: inline-block;
          padding: 8px 16px;
        }

        a:hover {
          background-color: #ddd;
          color: black;
        }

        .previous {
          background-color: #1A73E8;
          color: white;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>Export Products Data of Prepcenter App | <a href="products.php" class="previous">&laquo; Back to Product Page</a>
        </h2>
        <table name="productTable" id="productTable" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Item Name</th>
                    <th>MSKU</th>
                    <th>ASIN</th>
                    <th>FNSKU</th>
                    <th>PACK</th>
                    <th>QTY</th>
                </tr>
            </thead>
        </table>

    </div>
</body>
</html>