<?php

include "conn.php";

// $columnName = $_POST['columnName'];
// $sort = $_POST['sort'];

$columnName = 'date'; //tmp
$sort = 'ASC'; //tmp
$date_range = '2023-1-1_2023-12-31'; //tmp

//$date_range = $_GET['date_range'];
$date_range = date('Y-m-d').'_'.date('Y-m-d');
if(isset($_GET['date_range'])){
    $date_range = $_GET['date_range'];
}

$no = 1;

$daterange = '';
if(!empty($date_range)){
    $date_range = explode ("_", $date_range);

    $date_start = strtotime($date_range[0]);
    $mil = $date_start;
    $seconds = ceil($date_start / 1000);
    $date_start_conv = date("Y-m-d", $date_start);

    $date_end = strtotime($date_range[1]);
    $mil = $date_end;
    $seconds = ceil($date_end / 1000);
    $date_end_conv = date("Y-m-d", $date_end);
}

echo $date_start_conv."<br/>".$date_end_conv."<br />"; //tmp

$daterange = " WHERE daily_input.date BETWEEN '" . $date_start_conv . "' AND '" . $date_end_conv . "'";
$sql = "
  SELECT 
   daily_input.date, 
   employee.first_name, 
   employee.last_name, 
   daily_input.start_time, 
   daily_input.endtime, 
   daily_input.total_time_in_sec, 
   employee.rate, 
   daily_input.total_paid, 
   (SELECT SUM(daily_input_detail.qty) GROUP BY daily_input_detail.id_daily_input) AS total_qty_daily,
   daily_input.total_packing_cost,
   daily_input.total_item_hour
  FROM daily_input
  INNER JOIN employee ON daily_input.id_employee = employee.id
  INNER JOIN daily_input_detail ON daily_input.id = daily_input_detail.id_daily_input
  $daterange
  GROUP BY daily_input.id 
  ORDER BY ".$columnName." ".$sort." ";

$result = mysqli_query($koneksi,$sql);

$html = '';
while($row = mysqli_fetch_array($result)){
  $date = $row['date'];
  $first_name = $row['first_name'];
  $last_name = $row['last_name'];
  $start_time = $row['start_time'];
  $endtime = $row['endtime'];
  $rate = $row['rate'];
  $total_time_in_sec = $row['total_time_in_sec'];
    $hours = $total_time_in_sec / 60;
    $mins = $hours % 60;
    $hours = $hours / 60;

    echo $timeex = ((int)$hours . " H " . (int)$mins . " m"); //ultimate time display  
  $total_paid = $row['total_paid'];
  $total_qty_daily = $row['total_qty_daily'];
  $total_packing_cost = $row['total_packing_cost'];
  $total_item_hour = $row['total_item_hour'];

  $html .= "<tr>
    <td>".$no++."</td>
    <td>".$date."</td>
    <td>".$first_name." ".$last_name."</td>
    <td>".$start_time."</td>
    <td>".$endtime."</td>
    <td>$".$rate."</td>
    <td>".$timeex."</td>
    <td>$".$total_paid."</td>
    <td>".$total_qty_daily."</td>
    <td>$".$total_packing_cost."</td>
    <td>".$total_item_hour."</td>    
  </tr>";
}

echo $html;
?>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
$(function() {

    <?php
    if(isset($_GET['date_range'])){
      $date_range = explode ("_", $_GET['date_range']); 
    ?>
      var start = moment(new Date('<?php echo $date_range[0]; ?>'));
      // var start = moment();//moment().subtract(29, 'days');
      var end = moment(new Date('<?php echo $date_range[1]; ?>'));
      // var end = moment();

      // var start = moment(new Date('<?php echo $date_start_conv; ?>'));
      // // var start = moment();//moment().subtract(29, 'days');
      // var end = moment(new Date('<?php echo $date_end_conv; ?>'));
      // // var end = moment();

    <?php
    } else {
    ?>
      var start = moment();//moment().subtract(29, 'days');
      var end = moment();
    <?php
    }
    ?>

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $("#date_range").val(start.format('YYYY-M-D') + '_' + end.format('YYYY-M-D'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Last 3 Months': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')],
           'Last 6 Months': [moment().subtract(5, 'month').startOf('month'), moment().endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
           'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

    cb(start, end);

});
</script>