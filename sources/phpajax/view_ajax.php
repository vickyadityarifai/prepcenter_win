<?php
	include '../conn.php';
	if(isset($_GET['submit'])){
		$employee_id = $_GET['id'];
		$month = $_GET['month'];
		$sql = "SELECT * FROM daily_input WHERE id_employee = '".$employee_id."' AND date LIKE '%".$month."%' ORDER BY date ASC";
		$result = $koneksi->query($sql);
	} else {
		$sql = "SELECT * FROM daily_input ORDER BY id DESC";
		$result = $koneksi->query($sql);		
	}
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
?>	
		<tr>
<!-- 			<td>
				<p class="text-s font-weight-normal mb-0 ps-4 p-0">
					<?=$row['id'];?>
				</p>
			</td> -->
			<td>
				<p class="text-s font-weight-normal mb-0 p-0">
				</p>
			</td>
			<td>
				<div class="d-flex align-items-center">
					<p class="text-s font-weight-normal mb-0 ps-1 p-0">
						<?php
							$date 	= $row['date'];
							$day 	= date('l', strtotime($date));
							echo $day.", ".date('d M Y', strtotime($date));
						?>
					</p>
				</div>
			</td>
			<td>
				<div class="d-flex align-items-center">
					<span class="text-dark text-xs">
						<p class="text-s font-weight-normal mb-0 ps-2"><?=$row['start_time'];?></p>
					</span>
				</div>
			</td>
			<td>
				<div class="d-flex align-items-center">
					<span class="text-dark text-xs">
						<p class="text-s font-weight-normal mb-0 ps-1"><?=$row['endtime'];?></p>
					</span>
				</div>
			</td>
			<td>
				<div class="d-flex align-items-center">
					<span class="text-dark text-xs">
						<p class="text-s font-weight-normal mb-0 ps-1 p-0">
							<?php
								$time 	= $row['total_time_in_sec'];
			                    $hours 	= $time / 60;
			                    $mins 	= $hours % 60;
			                    $hours 	= $hours / 60;

			                    echo $timeex = ((int)$hours . " H " . (int)$mins . " m"); //ultimate time display
							?>

						</p>
					</span>
				</div>
			</td>
			<td>
				<button type="button" class="btn btn-info btn-sm update my-0 ps-1" data-toggle="modal" data-keyboard="false" data-backdrop="static" data-target="#update_time" data-id="<?=$row['id'];?>"
				data-date="<?=$row['date'];?>"
				data-start_time="<?=$row['start_time'];?>"
				data-endtime="<?=$row['endtime'];?>"
				data-total_time_in_sec="<?=$row['total_time_in_sec'];?>"
				">Update Time
				</button>
			</td>
		</tr>
<?php	
		}
	} else {
		echo "<tr >
		<td colspan='5'>No Result found !</td>
		</tr>";
	}
	mysqli_close($koneksi);
?>