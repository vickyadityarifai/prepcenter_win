<!DOCTYPE html>
<html lang="en">
<head>
  <title>View Ajax</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h2>View data</h2>
  <div class="alert alert-success alert-dismissible" id="success" style="display:none;">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	</div>
	<table class="table table-bordered table-sm" >
    <thead>
      <tr>
        <th>ID</th>
        <th>Date</th>
        <th>Start Time</th>
				<th>End Time</th>
				<th>Total Time</th>
				<th>Action</th>
      </tr>
    </thead>
    <tbody id="table">
      
    </tbody>
  </table>
</div>
<!-- Modal Update-->
    <div class="modal fade" id="update_time" role="dialog">
		<div class="modal-dialog modal-sm">
		  <div class="modal-content">
			<div class="modal-header" style="color:#fff;background-color: #e35f14;padding:6px;">
			  <h5 class="modal-title"><i class="fa fa-edit"></i> Update Time</h5>
			 
			</div>
			<div class="modal-body">
			
				<!--1-->
				<div class="row">
					<div class="col-md-3">
					    <label>ID</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="id_modal" id="id_modal" class="form-control-sm" disabled>
					</div>	
				</div>
			    <!--2-->
				<div class="row">
					<div class="col-md-3">
					    <label>Date</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="date_modal" id="date_modal" class="form-control-sm" disabled>
					</div>	
				</div>
			    <!--3-->
				<div class="row">
					<div class="col-md-3">
					    <label>Start Time</label>
					</div>
					<div class="col-md-9">
						<input type="time" name="start_time_modal" id="start_time_modal" class="form-control-sm" required>
					</div>	
				</div>
				<!--4-->
				<div class="row">
					<div class="col-md-3">
					    <label>End Time</label>
					</div>
					<div class="col-md-9">
						<input type="time" name="endtime_modal" id="endtime_modal" class="form-control-sm" required>
					</div>	
				</div>
				<input type="hidden" name="id_modal" id="id_modal" class="form-control-sm">
			</div>
			<div class="modal-footer" style="padding-bottom:0px !important;text-align:center !important;">
			<p style="text-align:center;float:center;"><button type="submit" id="update_time_button" class="btn btn-default btn-sm" style="background-color: #e35f14;color:#fff;">Save</button>
			<button type="button" class="btn btn-default btn-sm" data-dismiss="modal" style="background-color: #e35f14;color:#fff;">Close</button></p>
			
		  </div>
		  </div>
		</div>
	</div>
<!-- Modal End-->
<script>
$(document).ready(function() {
	$.ajax({
		url: "view_ajax.php",
		type: "POST",
		cache: false,
		success: function(dataResult){
			$('#table').html(dataResult); 
		}
	});
	$(function () {
		$('#update_time').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget); /*Button that triggered the modal*/
			var id = button.data('id');
			var date = button.data('date');
			var start_time = button.data('start_time');
			var endtime = button.data('endtime');
			var total_time_in_sec = button.data('total_time_in_sec');
			var modal = $(this);
			modal.find('#id_modal').val(id);
			modal.find('#date_modal').val(date);
			modal.find('#start_time_modal').val(start_time);
			modal.find('#endtime_modal').val(endtime);
			modal.find('#total_time_in_sec_modal').val(total_time_in_sec);
		});
    });
	$(document).on("click", "#update_time_button", function() { 
		$.ajax({
			url: "update_ajax.php",
			type: "POST",
			cache: false,
			data:{
				id: $('#id_modal').val(),
				date: $('#date_modal').val(),
				start_time: $('#start_time_modal').val(),
				endtime: $('#endtime_modal').val(),
				total_time_in_sec: $('#total_time_in_sec_modal').val(),
			},
			success: function(dataResult){
				var dataResult = JSON.parse(dataResult);
				if(dataResult.statusCode==200){
					$('#update_time').modal().hide();
					alert('Data updated successfully !');
					location.reload();					
				}
			}
		});
	}); 
});
</script>
</body>
</html>