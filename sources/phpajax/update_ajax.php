<?php
	include '../conn.php';
	$id=$_POST['id'];
	$date=$_POST['date'];
	$start_time=$_POST['start_time'];
	$endtime=$_POST['endtime'];

	//get employee rate and total paid
	$getrate = mysqli_query($koneksi, "
	  SELECT * FROM daily_input
	  INNER JOIN employee ON daily_input.id_employee = employee.id
	  WHERE daily_input.id = '$id'
	  ");
	while ($a = mysqli_fetch_array($getrate)){

	  $init = (strtotime($endtime) - strtotime($start_time));
	  $hour = round(($init / 3600)); //hour
	  $minute = round(($init / 60) % 60); //minute                
	  if ($init > 60 AND $init < 3600) {
	    $time = "0 H ".($minute % 60)." m";
	  } else if ($init > 3600 AND $init < 24*60*60) {
	    $time = ($hour % 24)." H ".($minute % 60)." m";
	  }
	  // echo "<br/ >Init mentah : ".$init.
	  //     "<br />Paid : $".
	      $total_paid = round(($init / 3600) * $a['rate'], 2);
	}

	//get qty daily
	$sql = mysqli_query($koneksi, "
	  SELECT SUM(daily_input_detail.qty) AS total_qty
	  FROM daily_input_detail
	  INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
	  WHERE daily_input_detail.id_daily_input = '".$id."' AND daily_input.date = '".$date."'
	  ");

	while($totalqtydaily = mysqli_fetch_array($sql)){
	  $totalqtydailytocount = $totalqtydaily['total_qty'];
	}

	if ($totalqtydailytocount > 0) {
	  $total_packing_cost = round($total_paid/$totalqtydailytocount, 2);
	  $total_item_hour = round($totalqtydailytocount / ($init / 3600), 2);
	} else {
	  $total_packing_cost = 0;
	  $total_item_hour = 0;
	}	

	$sql = "UPDATE daily_input SET start_time = '$start_time', endtime = '$endtime', total_time_in_sec = '$init', total_paid = '$total_paid', total_packing_cost = '$total_packing_cost', total_item_hour = '$total_item_hour' WHERE id = '".$id."' ";
	
	if (mysqli_query($koneksi, $sql)) {
		echo json_encode(array("statusCode"=>200));
	} 
	else {
		echo json_encode(array("statusCode"=>201));
	}
	mysqli_close($koneksi);
?>