<?php 

session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: login.php");
    exit; // prevent further execution, should there be more code that follows
}

include '../sources/conn.php';
include '../sources/excel_reader2.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="../image/png" href="../assets/img/favicon.png">
  <title>
    Prepcenter
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
</head>
<body>
<?php

$target = basename($_FILES['importfile']['name']) ;
move_uploaded_file($_FILES['importfile']['tmp_name'], $target);
 
chmod($_FILES['importfile']['name'],0777);
 
$data = new Spreadsheet_Excel_Reader($_FILES['importfile']['name'],false);
$jumlah_baris = $data->rowcount($sheet_index=0);
 
$berhasil = 0;
for ($i=2; $i<=$jumlah_baris; $i++){
 
	$title	= $data->val($i, 1);
	$msku 	= $data->val($i, 2);
	$asin	= $data->val($i, 3);
	$fnsku	= $data->val($i, 4);
 
	if($title != "" && $msku != "" && $asin != "" && $fnsku != ""){
		$sql = mysqli_query($koneksi,"INSERT INTO import_result values('','$title','$msku','$asin','$fnsku',0,0)");
		$berhasil++;
	}
}
 
unlink($_FILES['importfile']['name']);

echo "<div class='alert alert-success alert-dismissible text-white fade show my-4' role='alert' align='center'>
              <span><strong>Success!</strong> Data has been saved !!</span>
              <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>";
echo "<div class='alert alert-success alert-dismissible text-white fade show my-4' role='alert' align='center'>
              <button type='button' class='btn bg-gradient-info'>
                <a href = 'products.php?alert=success'>Click here to Continue!!!</a>
              </button>
            </div>";
 
//header("location:products.php?alert=success");

?>

</body>
</html>