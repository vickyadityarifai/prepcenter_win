<?php 

if(!isset($_SESSION)) 
{ 
    session_start(); 
}

  //session_start();

  // do check
  if (!isset($_SESSION["username"])) {
      header("location: ../login.php");
      exit; // prevent further execution, should there be more code that follows
  }

  include 'conn.php';
//include 'sources\conn.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <title>
    Prepcenter
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
</head>

<body class="g-sidenav-show  bg-gray-200">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="index.php">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Prepcenter</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white active bg-gradient-info" href="index.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li>
          <a class="nav-link text-white" href="daily_input.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">add_task</i>
            </div>
            <span class="nav-link-text ms-1">Daily Input</span>
          </a>          
        </li>
        <?php
          if ($_SESSION['role_id'] == '1'){
              echo "
              <li class='nav-item'>
                <a class='nav-link text-white' href='employee.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>table_view</i>
                  </div>
                  <span class='nav-link-text ms-1'>Employee</span>
                </a>
              </li> 
              <li class='nav-item'>
                <a class='nav-link text-white' href='products.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>inventory</i>
                  </div>
                  <span class='nav-link-text ms-1'>Products</span>
                </a>
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Report pages</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Employee</span>
                </a> 
              </li>
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage_time.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Time</span>
                </a> 
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Summary</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='monthly_date_summary.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>app_registration</i>
                  </div>
                  <span class='nav-link-text ms-1'>Mothly Date Summary</span>
                </a> 
              </li>
              ";
          } else {
              echo "";
          }
        ?>
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-info mt-4 w-100" href="logout.php" type="button">Log Out</a>
      </div>
    </div>    
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" data-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Dashboard</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Dashboard</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="#" class="nav-link text-body font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">
                  <?php echo $_SESSION['username']; ?>
                </span>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-right-from-bracket cursor-pointer"></i>
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="logout.php">
                    <div class="d-flex py-1">
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span>Log Out Now</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          <div id="current_date"></p>
                          <script>
                          document.getElementById("current_date").innerHTML = Date();
                          </script>                          
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->
<div class="container-fluid py-4">

      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
          <div class="card">
            <div class="card-header p-3 pt-2">
              <div class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                <i class="material-icons opacity-10">inventory</i>
              </div>
              <div class="text-end pt-1">
                <p class="text-sm mb-0 text-capitalize">Amount Product's</p>
                <h4 class="mb-0">

                <?php
                  $query = mysqli_query($koneksi, "SELECT count(DISTINCT fnsku) AS amount_product FROM import_result");
                  while ($d = mysqli_fetch_array($query)) {
                    echo $d['amount_product'];
                  }
                ?>

                </h4>
              </div>
            </div>
            <hr class="dark horizontal my-0">
            <div class="card-footer p-3">
              <p class="mb-0">
                <span class="text-success text-sm font-weight-bolder">

<!--                 <?php
                  $query = mysqli_query($koneksi, "SELECT sum(qty) AS amount_product_qty FROM import_result");
                  while ($d = mysqli_fetch_array($query)) {
                    echo $d['amount_product_qty'];
                  }
                ?>                

                </span>QTY recorded on system</p> -->
            </div>
          </div>
        </div>
        <?php
          if ($_SESSION['role_id'] == '1'){
              echo "
              <div class='col-xl-3 col-sm-6 mb-xl-0 mb-4'>
                <div class='card'>
                  <div class='card-header p-3 pt-2'>
                    <div class='icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute'>
                      <i class='material-icons opacity-10'>person</i>
                    </div>
                    <div class='text-end pt-1'>
                      <p class='text-sm mb-0 text-capitalize'>Amount Employee</p>
                      <h4 class='mb-0'>
              ";
          } else {
              echo "";
          }
                        $query = mysqli_query($koneksi, 'SELECT count(id) AS amount_employee FROM employee');
                        while ($d = mysqli_fetch_array($query)) {
                          if ($_SESSION['role_id'] == '1'){
                            echo $d["amount_employee"];
                          } else {
                            echo "";
                          }
                        }

          if ($_SESSION['role_id'] == '1'){
              echo "
                      </h4>
                    </div>
                  </div>
                  <hr class='dark horizontal my-0'>
                  <div class='card-footer p-3'>
                    <p class='mb-0'>
                    
              ";
          } else {
              echo "";
          }
                        // $query = mysqli_query($koneksi, "SELECT count(role_id) AS role_1_employee FROM employee WHERE role_id = 1");
                        // while ($d = mysqli_fetch_array($query)) {
                        //   if ($_SESSION['role_id'] == '1'){
                        //     echo $d['role_1_employee']."'";
                        //   } else {
                        //     echo "";
                        //   }
                        // }

          if ($_SESSION['role_id'] == '1'){
              echo "
                  </div>
                </div>
              </div>
              <div class='col-xl-3 col-sm-6 mb-xl-0 mb-4'>
                <div class='card'>
                  <div class='card-header p-3 pt-2'>
                    <div class='icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute'>
                      <i class='material-icons opacity-10'>schedule</i>
                    </div>
                    <div class='text-end pt-1'>
                      <p class='text-sm mb-0 text-capitalize'>Amount Time Working</p>
                      <h4 class='mb-0'>
              ";
          } else {
              echo "";
          }
              $sql = mysqli_query($koneksi,"
                SELECT 
                    employee.last_name, 
                    employee.first_name, 
                    employee.rate, 
                    daily_input.id_employee, 
                    SUM(daily_input.total_time_in_sec) 
                    AS Total_Time_In_Sec_Selected,
                    SUM(daily_input.total_paid)
                    AS Total_paid_selected,
                    SUM(daily_input.total_packing_cost)
                    AS Total_packing_cost_selected
                FROM employee 
                INNER JOIN daily_input
                ON employee.id = daily_input.id_employee
              ");

              while ($d = mysqli_fetch_array($sql)){

                  // $total_time_from_query = $d['Total_Time_In_Sec_Selected'];
                  // $hour = round(($total_time_from_query / 3600)); //hour
                  // $minute = round(($total_time_from_query / 60) % 60); //minute                
                  //   if ($_SESSION['role_id'] == '1'){
                  //     echo $timeex = ($hour % 24)." H ".($minute % 60)." m";
                  //   } else {
                  //     echo "";
                  //   }


                  $total_time_from_query = $d['Total_Time_In_Sec_Selected'];
                  $hour = round(($total_time_from_query / 3600)); //hour
                  $minute = round(($total_time_from_query / 60) % 60); //minute                
                  if ($total_time_from_query > 60 AND $total_time_from_query < 3600) {
                    $timeex = "0 H ".($minute % 60)." m";
                  } else if ($total_time_from_query > 3600 AND $total_time_from_query < 24*60*60) {
                    $timeex = ($hour % 24)." H ".($minute % 60)." m";
                  } else {
                    $timeex = ($hour)." H ".($minute % 60)." m";
                  }

                  if ($_SESSION['role_id'] == '1'){
                    echo $timeex;
                  } else {
                    echo "";
                  }

              }

          if ($_SESSION['role_id'] == '1'){
              echo "
                      </h4>
                    </div>
                  </div>
                  <hr class='dark horizontal my-0'>
                  <div class='card-footer p-3'>
                  </div>
                </div>
              </div>
              <div class='col-xl-3 col-sm-6'>
                <div class='card'>
                  <div class='card-header p-3 pt-2'>
                    <div class='icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute'>
                      <i class='material-icons opacity-10'>payments</i>
                    </div>
                    <div class='text-end pt-1'>
                      <p class='text-sm mb-0 text-capitalize'>Amount Payable</p>
                      <h4 class='mb-0'>
              ";
          } else {
              echo "";
          }
              $sql = mysqli_query($koneksi,"
                SELECT 
                    employee.last_name, 
                    employee.first_name, 
                    employee.rate, 
                    daily_input.id_employee, 
                    SUM(daily_input.total_time_in_sec) 
                    AS Total_Time_In_Sec_Selected,
                    SUM(daily_input.total_paid)
                    AS Total_paid_selected,
                    SUM(daily_input.total_packing_cost)
                    AS Total_packing_cost_selected
                FROM employee 
                INNER JOIN daily_input
                ON employee.id = daily_input.id_employee
              ");

              while ($d = mysqli_fetch_array($sql)){
                    $amount = round($d['Total_paid_selected'],2);
                    if ($_SESSION['role_id'] == '1'){
                      echo "$".$amount;
                    } else {
                      echo "";
                    }
              }

          if ($_SESSION['role_id'] == '1'){
              echo "
                      </h4>
                    </div>
                  </div>
                  <hr class='dark horizontal my-0'>
                  <div class='card-footer p-3'>
                    <p class='mb-0'>

              ";
          } else {
              echo "";
          }

          if ($_SESSION['role_id'] == '1'){
              echo "
                  </div>
                </div>
              </div>

              ";
          } else {
              echo "";
          }
        ?>
      </div>

      <?php
      if ($_SESSION['role_id'] == '1'){
        echo "
        <div class='row mt-4 mx-0'>
          <div class='card my-4'>
            <div class='card-header p-0 position-relative mt-n4 mx-3 z-index-2'>
              <div class='bg-gradient-info shadow-info border-radius-lg pt-2 pb-1'>
                <div><h6 class='text-white text-capitalize ps-3' align='text-center'><i class='material-icons'>receipt_long</i> Top Total Packing Cost Per Item</h6></div>
              </div>
            </div>
              <table class='table align-items-center mb-0'>
                <thead>
                  <tr>
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7'>Working Date</th>
                    <th class='text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2'>Employee Name</th>
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-0'>Start Time</th>
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-0'>End Time</th>
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-0'>Rate / Hour</th>
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-0'>Total Time</th>            
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2'>Total Paid</th>
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2'>QTY</th>
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2'>PC / Item</th>
                    <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2'>Item / Hour</th>                  
                  </tr>
                </thead>
                <tbody>";

                    $data = mysqli_query($koneksi,"
                      SELECT 
                        employee.first_name, 
                        employee.last_name, 
                        employee.rate, 
                        daily_input.id, 
                        daily_input.date, 
                        daily_input.start_time, 
                        daily_input.endtime, 
                        daily_input.total_time_in_sec, 
                        daily_input.total_paid, 
                        daily_input.total_packing_cost, 
                        daily_input.total_item_hour
                      FROM employee
                      INNER JOIN daily_input ON employee.id = daily_input.id_employee
                      ORDER BY daily_input.total_packing_cost DESC LIMIT 5
                    ");

                    while($d = mysqli_fetch_array($data)){
                      $id = $d['id'];
                      $date = $d['date'];
                      $employee_name = $d['last_name']." ".$d['first_name'];
                      $start_time = $d['start_time'];
                      $endtime = $d['endtime'];
                      $rate = $d['rate'];
                      $total_time_in_sec = $d['total_time_in_sec'];
                      $total_paid = $d['total_paid'];
                      $total_packing_cost = $d['total_packing_cost'];
                      $total_item_hour = $d['total_item_hour'];

        echo "          
                  <tr>
                    <td>
                      <div class='d-flex align-items-center'>
                        <p class='text-xs font-weight-normal mb-0 ps-7'>"; $day = date('D', strtotime($date)); echo $day.', '.date('F d, Y', strtotime($date)); 
        echo "          </p>
                      </div>
                    </td>
                    <td>
                      <div class='d-flex'>
                        <div class='my-auto'>
                          <h6 class='mb-0 text-xs'>";
        echo $employee_name; 
        echo "            </h6>
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class='d-flex align-items-center'>
                        <span class='text-dark text-xs'>
                          <p class='text-xs font-weight-normal mb-0 ps-1'>";
        echo $start_time;
        echo "            </p>
                        </span>
                      </div>
                    </td>
                    <td>
                      <div class='d-flex align-items-center'>
                        <span class='text-dark text-xs'>
                          <p class='text-xs font-weight-normal mb-0 ps-1'>";
        echo $endtime;
        echo "            </p>
                        </span>
                      </div>
                    </td>
                    <td>
                      <div class='d-flex align-items-center'>
                        <span class='text-dark text-xs'>
                          <p class='text-xs font-weight-normal mb-0 ps-4'>";
        echo "$".$rate; 
        echo "            </p>
                        </span>
                      </div>
                    </td>
                    <td>
                      <div class='d-flex align-items-center'>
                        <span class='text-dark text-xs'>
                          <p class='text-xs font-weight-normal mb-0 ps-2'>";

                              $time = $total_time_in_sec; 
                              $hours = $time / 60;
                              $mins = $hours % 60;
                              $hours = $hours / 60;

                              echo $timeex = ((int)$hours . " H " . (int)$mins . " m"); //ultimate time display
                            
        echo "            </p>
                        </span>
                      </div>
                    </td>          
                    <td>
                      <div class='d-flex align-items-center'>
                        <span class='text-dark text-xs'>
                          <p class='text-xs font-weight-normal mb-0 ps-4'>";
        echo "$".$total_paid;
        echo "            </p>
                        </span>
                      </div>
                    </td>
                    <td>
                      <div class='d-flex align-items-center'>
                        <span class='text-dark text-xs'>
                          <p class='text-xs font-weight-normal mb-0 ps-3'>";

                              $qty_display = 0;

                              $qty = 
                                  "
                                  SELECT SUM(daily_input_detail.qty) AS total_qty
                                  FROM daily_input_detail
                                  INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
                                  WHERE daily_input_detail.id_daily_input = '".$id."' 
                                  ";
                              $queryqty = mysqli_query($koneksi, $qty);
                              while ($qtyshow = mysqli_fetch_array($queryqty)) {
                                $qty_display = $qtyshow['total_qty'];
                              }
                              
                              if ($qty_display > 0) {
                                echo $qty_display;
                              } else {
                                echo "<b style='color:red;'>0</b>";
                              }

        echo "            </p>
                        </span>
                      </div>
                    </td>
                    <td>
                      <div class='d-flex'>
                        <div class='my-auto'>
                          <h6 class='mb-0 text-xs ps-3'>";
        echo "$".$total_packing_cost;
        echo "            </h6>
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class='d-flex'>
                        <div class='my-auto'>
                          <h6 class='mb-0 text-xs ps-4'>";
        echo $total_item_hour;
        echo "            </h6>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>";
                    }
        echo "  <tfoot>
                  <tr>
                    <td align='right' colspan='10'><a href='daily_input.php'>See More ...</a></td>
                  </tr>
                </tfoot>
              </table>
          </div>
        </div>";

      } else {
        echo "";
      }
      ?>

</div>
<footer class="footer py-4  ">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="copyright text-center text-sm text-muted text-lg-start">
          © <script>
            document.write(new Date().getFullYear())
          </script>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link pe-0 text-muted" target="_blank">License</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>
</main>
    <div class="fixed-plugin">
    <a class="fixed-plugin-button text-dark position-fixed px-3 py-2">
      <i class="material-icons py-2">settings</i>
    </a>
    <div class="card shadow-lg">
      <div class="card-header pb-0 pt-3">
        <div class="float-start">
          <h5 class="mt-3 mb-0">Material UI Configurator</h5>
          <p>See our dashboard options.</p>
        </div>
        <div class="float-end mt-4">
          <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <!-- End Toggle Button -->
      </div>
      <hr class="horizontal dark my-1">
      <div class="card-body pt-sm-3 pt-0">
        <!-- Sidebar Backgrounds -->
        <div>
          <h6 class="mb-0">Sidebar Colors</h6>
        </div>
        <a href="javascript:void(0)" class="switch-trigger background-color">
          <div class="badge-colors my-2 text-start">
            <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
          </div>
        </a>

        <!-- Sidenav Type -->
        
        <div class="mt-3">
          <h6 class="mb-0">Sidenav Type</h6>
          <p class="text-sm">Choose between 2 different sidenav types.</p>
        </div>

        <div class="d-flex">
          <button class="btn bg-gradient-dark px-3 mb-2 active" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
          <button class="btn bg-gradient-dark px-3 mb-2 ms-2" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
          <button class="btn bg-gradient-dark px-3 mb-2 ms-2" data-class="bg-white" onclick="sidebarType(this)">White</button>
        </div>

        <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>
    
        <!-- Navbar Fixed -->
        
        <div class="mt-3 d-flex">
          <h6 class="mb-0">Navbar Fixed</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)">
          </div>
        </div>
           
        <hr class="horizontal dark my-3">
        <div class="mt-2 d-flex">
          <h6 class="mb-0">Light / Dark</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)">
          </div>
        </div>
        <hr class="horizontal dark my-sm-4">
            
        <a class="btn bg-gradient-info w-100" href="https://www.creative-tim.com/product/material-dashboard-pro">Free Download</a>
            
        <a class="btn btn-outline-dark w-100" href="https://www.creative-tim.com/learning-lab/bootstrap/overview/material-dashboard">View documentation</a>
        
        <div class="w-100 text-center">
          <a class="github-button" href="https://github.com/creativetimofficial/material-dashboard" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star creativetimofficial/material-dashboard on GitHub">Star</a>
          <h6 class="mt-3">Thank you for sharing!</h6>
          
          <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
          </a>
              
          <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
          </a>
          
        </div>
      </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../assets/js/core/popper.min.js" ></script>
<script src="../assets/js/core/bootstrap.min.js" ></script>
<script src="../assets/js/plugins/perfect-scrollbar.min.js" ></script>
<script src="../assets/js/plugins/smooth-scrollbar.min.js" ></script>

<script>
  var win = navigator.platform.indexOf('Win') > -1;
  if (win && document.querySelector('#sidenav-scrollbar')) {
    var options = {
      damping: '0.5'
    }
    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
  }
</script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>


<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=3.0.4"></script>

<script src="../assets/js/plugins/chartjs.min.js"></script>
<script>
  var ctx = document.getElementById("chart-bars").getContext("2d");

  new Chart(ctx, {
    type: "bar",
    data: {
      labels: ["Hameed", "M Fahad", "Nancy", "Salman VA", "Sami", "Waleed"],
      datasets: [{
        label: "Total QTY By Employee",
        tension: 0.4,
        borderWidth: 0,
        borderRadius: 4,
        borderSkipped: false,
        backgroundColor: "rgba(255, 255, 255, .8)",
        data: [25, 67, 65, 23, 32, 34
                // <?php 
                //   //$qty_display = [];
                //   $get_last_qty = mysqli_query($koneksi, "
                //   SELECT id_daily_input, SUM(daily_input_detail.qty) AS total_qty 
                //   FROM daily_input_detail 
                //   INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input 
                //   GROUP BY daily_input_detail.id_daily_input DESC LIMIT 5;");
                //   while($last_qty_show = mysqli_fetch_array($get_last_qty)){
                //     $qty_display = $last_qty_show['total_qty'];
                //     // $qty_last = implode(",", $qty_display);
                //     // print_r($qty_last);
                //     //print_r($qty_display);
                //     $qty_last = implode(",", $qty_display).",";
                //     echo $qty_last;
                //   }
                // ?>
              ],
        maxBarThickness: 6
      }, ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: false,
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5],
            color: 'rgba(255, 255, 255, .2)'
          },
          ticks: {
            suggestedMin: 0,
            suggestedMax: 500,
            beginAtZero: true,
            padding: 10,
            font: {
              size: 14,
              weight: 300,
              family: "Roboto",
              style: 'normal',
              lineHeight: 2
            },
            color: "#fff"
          },
        },
        x: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5],
            color: 'rgba(255, 255, 255, .2)'
          },
          ticks: {
            display: true,
            color: '#f8f9fa',
            padding: 10,
            font: {
              size: 14,
              weight: 300,
              family: "Roboto",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });


  var ctx2 = document.getElementById("chart-line").getContext("2d");

  new Chart(ctx2, {
    type: "line",
    data: {
      labels: ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      datasets: [{
        label: "Mobile apps",
        tension: 0,
        borderWidth: 0,
        pointRadius: 5,
        pointBackgroundColor: "rgba(255, 255, 255, .8)",
        pointBorderColor: "transparent",
        borderColor: "rgba(255, 255, 255, .8)",
        borderColor: "rgba(255, 255, 255, .8)",
        borderWidth: 4,
        backgroundColor: "transparent",
        fill: true,
        data: [50, 40, 300, 320, 500, 350, 200, 230, 500],
        maxBarThickness: 6

      }],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: false,
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5],
            color: 'rgba(255, 255, 255, .2)'
          },
          ticks: {
            display: true,
            color: '#f8f9fa',
            padding: 10,
            font: {
              size: 14,
              weight: 300,
              family: "Roboto",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: false,
            drawOnChartArea: false,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#f8f9fa',
            padding: 10,
            font: {
              size: 14,
              weight: 300,
              family: "Roboto",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });

  var ctx3 = document.getElementById("chart-line-tasks").getContext("2d");

  new Chart(ctx3, {
    type: "line",
    data: {
      labels: ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      datasets: [{
        label: "Mobile apps",
        tension: 0,
        borderWidth: 0,
        pointRadius: 5,
        pointBackgroundColor: "rgba(255, 255, 255, .8)",
        pointBorderColor: "transparent",
        borderColor: "rgba(255, 255, 255, .8)",
        borderWidth: 4,
        backgroundColor: "transparent",
        fill: true,
        data: [50, 40, 300, 220, 500, 250, 400, 230, 500],
        maxBarThickness: 6

      }],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: false,
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5],
            color: 'rgba(255, 255, 255, .2)'
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#f8f9fa',
            font: {
              size: 14,
              weight: 300,
              family: "Roboto",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: false,
            drawOnChartArea: false,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#f8f9fa',
            padding: 10,
            font: {
              size: 14,
              weight: 300,
              family: "Roboto",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
</script>
</body>

</html>