<?php 
  session_start();
  // do check
  if (!isset($_SESSION["username"])) {
      header("location: login.php");
      exit; // prevent further execution, should there be more code that follows
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="../image/png" href="../assets/img/favicon.png">
  <title>
    Prepcenter
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
</head>

<body class="g-sidenav-show  bg-gray-200">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="../index.php">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Prepcenter</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white" href="../index.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="employee.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">table_view</i>
            </div>
            <span class="nav-link-text ms-1">Employee</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="products.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">inventory</i>
            </div>
            <span class="nav-link-text ms-1">Products</span>
          </a>
        </li>
        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Report pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white active bg-gradient-info" href="reportpage.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report Page</span>
          </a> 
        </li>        
        <li class="nav-item">
          <a class="nav-link text-white active bg-gradient-info" href="report.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report by Employee</span>
          </a> 
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="report_old.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report by Time</span>
          </a>
        </li>                
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-info mt-4 w-100" href="logout.php" type="button">Log Out</a>
      </div>
    </div>    
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" data-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Report</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Report</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="#" class="nav-link text-body font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">
                  <?php echo $_SESSION['username']; ?>
                </span>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-right-from-bracket cursor-pointer"></i>
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="logout.php">
                    <div class="d-flex py-1">
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span>Log Out Now</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          <div id="current_date"></p>
                          <script>
                          document.getElementById("current_date").innerHTML = Date();
                          </script>                          
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

<div class="container-fluid py-4">

<form method="get">
  <div class="row">
    <div class="col-md-2">
      <div class="input-group input-group-static my-3">
        <label>Select Start Date :</label>
          <input type="date" name="startdate" class="form-control">
      </div>
    </div>
    <div class="col-md-2">
      <div class="input-group input-group-static my-3">
        <label>Select End Date :</label>
          <input type="date" name="enddate" class="form-control">
      </div>
    </div>
    <div class="col-md-2">
      <div class="input-group input-group-static my-3">
        <label>Select Employee :</label>
          <select name="employee_name" class="form-control">
            <option value="kosong"> - All Employee - </option>
            <?php
              include 'conn.php';
              $data = mysqli_query($koneksi, "SELECT employee.last_name, employee.first_name, userlog.username FROM employee INNER JOIN userlog ON employee.first_name = userlog.username GROUP BY userlog.username");
              while ($d = mysqli_fetch_array($data)) {
            ?>
            <option value="<?php echo $d['username']; ?>"><?php echo $d['last_name'].' '.$d['first_name']; ?></option>
            <?php
              }
            ?>
          </select>
      </div>
    </div>
    <div class="col-md-1">
      <div class="input-group input-group-static my-4">
        <button type="submit" name="submit" class="btn bg-gradient-info">Set</button>
      </div>
    </div>
    <div class="col-md-3">
      <div class="input-group input-group-static my-4">
        <button type="reset" name="reset" class="btn bg-gradient-danger"><a href="report.php">Reset</a></button>
      </div>
    </div>
  </div>
</form>    

<div class="card">
  <div class="table-responsive">
    <table class="table align-items-center mb-0">
      <thead>
        <tr>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">No.</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Employee Name</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Rate / Hour</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Login Time</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Logout Time</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Total Time</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Amount Payable</th>                    
          <th></th>
        </tr>
      </thead>

    <?php 
    include 'conn.php'; //QUERY FOR SORT DATE FROM AND TO AND EMPLOYEE NAME
    $no=1;
    
    if(isset($_GET['submit'])){
      $startdate = $_GET['startdate'];
      $enddate = $_GET['enddate'];
      $employee = $_GET['employee_name'];
      $sql = mysqli_query($koneksi,"
        SELECT employee.last_name, employee.first_name, employee.rate, userlog.loginTime, userlog.logoutTime
        FROM employee 
        INNER JOIN userlog
        ON employee.first_name = userlog.username 
        WHERE userlog.loginTime AND userlog.logoutTime 
        BETWEEN '".$startdate." 00:00:01' AND '".$enddate." 23:59:00'
        AND userlog.username = '".$employee."'
        AND userlog.logoutTime != '0000-00-00 00:00:00' ");
    } else if(isset($_GET['submit'])){
      $startdate = $_GET['startdate'];
      $enddate = $_GET['enddate'];
      $employee = $_GET['employee_name'];
      $sql = mysqli_query($koneksi,"
        SELECT employee.last_name, employee.first_name, employee.rate, userlog.loginTime, userlog.logoutTime
        FROM employee 
        INNER JOIN userlog
        ON employee.first_name = userlog.username 
        WHERE userlog.loginTime AND userlog.logoutTime 
        BETWEEN '".$startdate." 00:00:01' AND '".$enddate." 23:59:00'
        AND userlog.username != '".$employee."'
        AND userlog.logoutTime != '0000-00-00 00:00:00' ");
    } else {
      $sql = mysqli_query($koneksi,"
        SELECT employee.last_name, employee.first_name, employee.rate, userlog.loginTime, userlog.logoutTime
        FROM employee 
        INNER JOIN userlog
        ON employee.first_name = userlog.username
        WHERE userlog.logoutTime != '0000-00-00 00:00:00' ");
    }   

    while($d = mysqli_fetch_array($sql)){
    ?>

      <tbody>
        <tr>
          <td>
            <p class="text-xs font-weight-normal mb-0"><?php echo $no++; ?></p>
          </td>          
          <td>
            <div class="d-flex px-2">
              <div class="my-auto">
                <h6 class="mb-0 text-xs"><?php echo $d['last_name'].' '.$d['first_name']; ?></h6>
              </div>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <p class="text-xs font-weight-normal mb-0"><?php echo '$'.$d['rate']; ?></p>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <?php 
//                  if(isset($_GET['submit'])){
//                    $startdate = $_GET['startdate'];
//                    $enddate = $_GET['enddate'];
//                    $total_time_sec = '';
//                    $query = 
//                    "SELECT SUM(TIME_TO_SEC(TIMEDIFF(subquery.logoutTime, subquery.loginTime))) AS Total_Time_Sec
//                    FROM ( 
//                        SELECT * 
//                        FROM `userlog`
//                        WHERE `loginTime` BETWEEN '".$startdate." 00:00:01' AND '".$startdate." 23:59:00' 
//                        AND `logoutTime` BETWEEN '".$enddate." 00:00:01' AND '".$enddate." 23:59:00' 
//                        AND username = '".$d['first_name']."' 
//                    ) AS subquery";                  
//                    
//                    $total_time_sec = mysqli_query($koneksi, $query) or die( mysqli_error($db));
//
//                  echo $total_time_sec;
//                  }

                  // while($a = mysqli_fetch_array($total_time_sec)){                  
                  //   echo $a['Total_Time_Sec'];
                  // }

                ?>
                <?php echo $d['loginTime']; ?>
              </span>
            </div>
          </td>
          <td class="align-middle text-center">
            <div class="d-flex align-items-center">
              <span class="me-2 text-xs"><?php echo $d['logoutTime']; ?></span>
            </div>
          </td>
          <td class="align-right text-right">
            <div class="align-items-right">
              <span class="me-2 text-xs">
              <?php 
              
                $to_time = strtotime($d['loginTime']);
                $from_time = strtotime($d['logoutTime']);
                // $second = ($from_time - $to_time); //second
                // $minute = round($second / 60); //minute
                // $hour = round(($second / 60 / 60)); //hour
                $init = ($from_time - $to_time);
                $second = $init % 60; //second
                $hour = round(($init / 3600)); //hour
                $minute = round(($init / 60) % 60); //minute                
                if ($init <= 60) {
                  echo $time = "0 H 0 m ".$init." s";
                } else if ($init > 60 AND $init < 3600) {
                  echo $time = "0 H ".($minute % 60)." m ".($init % 60)." s";
                } else if ($init > 3600 AND $init < 24*60*60) {
                  echo $time = ($hour % 24)." H ".($minute % 60)." m ".($init % 60)." s";
                }
                //echo $hour." H ".$minute." m";
              ?>
              </span>
            </div>
          </td>
          <td class="align-middle text-center">
            <div class="d-flex align-items-center">
              <span class="me-2 text-xs">
              <?php 
              
                $amount = round(($init / 3600) * $d['rate'], 2);
                echo "$".$amount;

              ?></span>
            </div>
          </td>                    
          <td class="align-middle">
            <button class="btn btn-link text-secondary mb-0">
              <span class="material-icons">
              more_vert
              </span>
            </button>
          </td>
        </tr>
      </tbody>
      <?php
      }
      ?>
      <!--TABEL ROW TO SEE THE TOTAL TIME AND AMOUNT PAYABLE BASED START AND END DATE AND BY EMPLOYEE-->
      <tr>
      <?php
      if(isset($_GET['submit'])){
        $startdate = $_GET['startdate'];
        $enddate = $_GET['enddate'];
        $employee = $_GET['employee_name'];
        // $sql = mysqli_query($koneksi,"
        //   SELECT employee.last_name, employee.first_name, employee.rate, userlog.loginTime, userlog.logoutTime
        //   FROM employee 
        //   INNER JOIN userlog
        //   ON employee.first_name = userlog.username where userlog.loginTime and userlog.logoutTime 
        //   BETWEEN '".$startdate." 00:00:01' AND '".$enddate." 23:59:00'
        //   GROUP BY userlog.loginTime AND userlog.logoutTime
        //   "); 
        //***SQL diatas mengelompokkan berdasarkan waktu login dan logout, tapi total time dan total amount masih belum sync
        // $sql = mysqli_query($koneksi,"
        //   SELECT employee.last_name, employee.first_name, employee.rate, userlog.loginTime, userlog.logoutTime
        //   FROM employee 
        //   INNER JOIN userlog
        //   ON employee.first_name = userlog.username 
        //   WHERE userlog.loginTime AND userlog.logoutTime 
        //   BETWEEN '".$startdate." 00:00:01' AND '".$enddate." 23:59:00'
        //   ");
        //***SQL diatas hanya menampilkan semua, tanpa filter waktu, amout
        $sql = mysqli_query($koneksi,"
          SELECT 
              employee.last_name, 
              employee.first_name, 
              employee.rate, 
              userlog.loginTime, 
              userlog.logoutTime,
              TIMEDIFF(userlog.logoutTime, userlog.loginTime) 
              AS Total_Time,
              TIME_TO_SEC(TIMEDIFF(userlog.logoutTime, userlog.loginTime)) 
              AS Total_Time_In_Sec,
              SUM(TIME_TO_SEC(TIMEDIFF(userlog.logoutTime, userlog.loginTime))) 
              AS Total_Time_In_Sec_Selected,
              SUM(((TIME_TO_SEC(TIMEDIFF(userlog.logoutTime, userlog.loginTime)) / 3600) * employee.rate ))
              AS Total_Amount
          FROM employee 
          INNER JOIN userlog
          ON employee.first_name = userlog.username 
          WHERE userlog.loginTime AND userlog.logoutTime 
          BETWEEN '".$startdate." 00:00:01' AND '".$enddate." 23:59:00'
          AND userlog.username = '".$employee."' 
          ");
        //***SQL diatas bisa menjumlahkan total waktu(dalam detik) berdasarkan pilihan waktu
      } else {
        // $startdate = $_GET['startdate'];
        // $enddate = $_GET['enddate'];
        $sql = mysqli_query($koneksi,"
          SELECT 
              employee.last_name, 
              employee.first_name, 
              employee.rate, 
              userlog.loginTime, 
              userlog.logoutTime,
              TIMEDIFF(userlog.logoutTime, userlog.loginTime) AS Total_Time,
              TIME_TO_SEC(TIMEDIFF(userlog.logoutTime, userlog.loginTime)) AS Total_Time_In_Sec,
              SUM(TIME_TO_SEC(TIMEDIFF(userlog.logoutTime, userlog.loginTime))) AS Total_Time_In_Sec_Selected,
              SUM(((TIME_TO_SEC(TIMEDIFF(userlog.logoutTime, userlog.loginTime)) / 3600) * employee.rate )) AS Total_Amount
          FROM employee 
          INNER JOIN userlog
          ON employee.first_name = userlog.username 
          WHERE userlog.logoutTime != '0000-00-00 00:00:00'");
      }    

      while($d = mysqli_fetch_array($sql)){ 
      ?>
        <td colspan="3" align="Center">
          <b>TOTAL</b>
        </td>
        <td>
          <b><?php
              if (!isset($_GET['submit'])){ 
                $startdate = mysqli_query($koneksi, 'SELECT @min_val:=min(loginTime) AS man_startdate FROM userlog'); 
                while ($row = mysqli_fetch_array($startdate)) {
                    echo $row['man_startdate'];
                }
              } else {
                echo $startdate." 00:00:00";
              }              
             ?>
          </b>
        </td>
        <td>
          <b><?php
              if (!isset($_GET['submit'])){ 
                $enddate = mysqli_query($koneksi, 'SELECT @max_val:=max(logoutTime) AS man_enddate FROM userlog'); 
                while ($row = mysqli_fetch_array($enddate)) {
                    echo $row['man_enddate'];
                }
              } else {          
                echo $enddate." 24:00:00";
              }          
             ?>
          </b>
        </td>        
        <td>
          <b>
            <?php
           
            $to_time = strtotime($d['loginTime']);
            $from_time = strtotime($d['logoutTime']);

            $total_time_from_query = $d['Total_Time_In_Sec_Selected'];
            $second = $total_time_from_query % 60; //second
            $hour = round(($total_time_from_query / 3600)); //hour
            $minute = round(($total_time_from_query / 60) % 60); //minute
            if ($total_time_from_query <= 60) {
              echo $time = "0 H 0 m ".$total_time_from_query." s";
            } else if ($total_time_from_query > 60 AND $total_time_from_query < 3600) {
              echo $time = "0 H ".($minute % 60)." m ".($total_time_from_query % 60)." s";
            } else if ($total_time_from_query > 3600 AND $total_time_from_query < 24*60*60) {
              echo $time = ($hour % 24)." H ".($minute % 60)." m ".($total_time_from_query % 60)." s";
            } else if ($total_time_from_query > 24*60*60) {
              echo $time = ($hour)." H ".($minute % 60)." m ".($total_time_from_query % 60)." s";
            } else {
              echo "Please Select the Start Date and End Date first above";
            }

            ?>
          </b>
        </td>
        <td>
          <b>
            <?php
                //$amount = round(($second / 60 / 60) * $d['rate'], 2); //TOTAL AMOUNT MASIH BELUM TERJUMLAH SESUAI WAKTU
                $amount = round($d['Total_Amount'],2);
                echo "$".$amount;
            ?>
          </b>
        </td>
      </tr>
      <?php
      }
      ?>
    </table>
  </div>
  </div>

</div>
<footer class="footer py-4  ">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="copyright text-center text-sm text-muted text-lg-start">
          © <script>
            document.write(new Date().getFullYear())
          </script>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link pe-0 text-muted" target="_blank">License</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>
</main>
    <div class="fixed-plugin">
    <a class="fixed-plugin-button text-dark position-fixed px-3 py-2">
      <i class="material-icons py-2">settings</i>
    </a>
    <div class="card shadow-lg">
      <div class="card-header pb-0 pt-3">
        <div class="float-start">
          <h5 class="mt-3 mb-0">Material UI Configurator</h5>
          <p>See our dashboard options.</p>
        </div>
        <div class="float-end mt-4">
          <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <!-- End Toggle Button -->
      </div>
      <hr class="horizontal dark my-1">
      <div class="card-body pt-sm-3 pt-0">
        <!-- Sidebar Backgrounds -->
        <div>
          <h6 class="mb-0">Sidebar Colors</h6>
        </div>
        <a href="javascript:void(0)" class="switch-trigger background-color">
          <div class="badge-colors my-2 text-start">
            <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
          </div>
        </a>

        <!-- Sidenav Type -->
        
        <div class="mt-3">
          <h6 class="mb-0">Sidenav Type</h6>
          <p class="text-sm">Choose between 2 different sidenav types.</p>
        </div>

        <div class="d-flex">
          <button class="btn bg-gradient-dark px-3 mb-2 active" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
          <button class="btn bg-gradient-dark px-3 mb-2 ms-2" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
          <button class="btn bg-gradient-dark px-3 mb-2 ms-2" data-class="bg-white" onclick="sidebarType(this)">White</button>
        </div>

        <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>
    
        <!-- Navbar Fixed -->
        
        <div class="mt-3 d-flex">
          <h6 class="mb-0">Navbar Fixed</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)">
          </div>
        </div>
           
        <hr class="horizontal dark my-3">
        <div class="mt-2 d-flex">
          <h6 class="mb-0">Light / Dark</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)">
          </div>
        </div>
        <hr class="horizontal dark my-sm-4">
            
        <a class="btn bg-gradient-info w-100" href="https://www.creative-tim.com/product/material-dashboard-pro">Free Download</a>
            
        <a class="btn btn-outline-dark w-100" href="https://www.creative-tim.com/learning-lab/bootstrap/overview/material-dashboard">View documentation</a>
        
        <div class="w-100 text-center">
          <a class="github-button" href="https://github.com/creativetimofficial/material-dashboard" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star creativetimofficial/material-dashboard on GitHub">Star</a>
          <h6 class="mt-3">Thank you for sharing!</h6>
          
          <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
          </a>
              
          <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
          </a>
          
        </div>
      </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="./assets/js/core/popper.min.js" ></script>
<script src="./assets/js/core/bootstrap.min.js" ></script>
<script src="./assets/js/plugins/perfect-scrollbar.min.js" ></script>
<script src="./assets/js/plugins/smooth-scrollbar.min.js" ></script>

<script>
  var win = navigator.platform.indexOf('Win') > -1;
  if (win && document.querySelector('#sidenav-scrollbar')) {
    var options = {
      damping: '0.5'
    }
    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
  }
</script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./assets/js/material-dashboard.min.js?v=3.0.4"></script>
</body>

</html>