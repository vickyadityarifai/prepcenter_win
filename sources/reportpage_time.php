<?php 

if(!isset($_SESSION)) 
{ 
    session_start(); 
}
  //session_start();
  // do check
  if (!isset($_SESSION["username"])) {
      header("location: ../login.php");
      exit; // prevent further execution, should there be more code that follows
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="../image/png" href="../assets/img/favicon.png">
  <title>
    Prepcenter
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
</head>

<body class="g-sidenav-show  bg-gray-200">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="index.php">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Prepcenter</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white" href="index.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li>
          <a class="nav-link text-white" href="daily_input.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">add_task</i>
            </div>
            <span class="nav-link-text ms-1">Daily Input</span>
          </a>          
        </li>        
        <?php
          if ($_SESSION['role_id'] == '1'){
              echo "
              <li class='nav-item'>
                <a class='nav-link text-white' href='employee.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>table_view</i>
                  </div>
                  <span class='nav-link-text ms-1'>Employee</span>
                </a>
              </li> 
              <li class='nav-item'>
                <a class='nav-link text-white' href='products.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>inventory</i>
                  </div>
                  <span class='nav-link-text ms-1'>Products</span>
                </a>
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Report pages</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Employee</span>
                </a> 
              </li>
              <li class='nav-item'>
                <a class='nav-link text-white active bg-gradient-info' href='reportpage_time.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Time</span>
                </a> 
              </li>                
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Summary</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='monthly_date_summary.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>app_registration</i>
                  </div>
                  <span class='nav-link-text ms-1'>Mothly Date Summary</span>
                </a> 
              </li>
              ";
          } else {
              echo "";
          }
        ?>
<!--         <li class="nav-item">
          <a class="nav-link text-white" href="report.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report by Employee</span>
          </a> 
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="report_old.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report by Time</span>
          </a>
        </li>                 -->
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-info mt-4 w-100" href="logout.php" type="button">Log Out</a>
      </div>
    </div>    
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" data-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Report By Time</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Report By Time</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="#" class="nav-link text-body font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">
                  <?php echo $_SESSION['username']; ?>
                </span>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-right-from-bracket cursor-pointer"></i>
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="logout.php">
                    <div class="d-flex py-1">
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span>Log Out Now</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          <div id="current_date"></p>
                          <script>
                          document.getElementById("current_date").innerHTML = Date();
                          </script>                          
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

<div class="container-fluid py-4">

<form method="get">
  <div class="row">
    <div class="col-md-4">
      <div class="input-group input-group-static my-3">
        <label>Select Date Range Report :</label>
          <?php
          $date_range = date('Y-m-d').'_'.date('Y-m-d');
          if(isset($_GET['date_range'])){
              $date_range = $_GET['date_range'];
          }
          ?>
          <input type="hidden" name="date_range" id="date_range" value="<?php echo $date_range; ?>">          
          <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
              <i class="fa fa-calendar"></i>&nbsp;
              <span><?php echo $date_range; ?></span> <i class="fa fa-caret-down"></i>
          </div>
      </div>
    </div>

    <div class="col-md-2" style="height: 50px; width: 104px;">
      <div class="input-group input-group-static my-4">
        <button type="submit" name="submit" class="btn bg-gradient-info">Search</button>
      </div>
    </div>
    <div class="col-md-2">
      <div class="input-group input-group-static my-4">
        <button type="reset" name="reset" class="btn bg-gradient-danger"><a href="reportpage_time.php">Clear</a></button>
      </div>
    </div>
  </div>
</form>    

<div class="card">
  <div class="table-responsive">

    <?php
    $columns = array('date');
    $column = isset($_GET['column']) && in_array($_GET['column'], $columns) ? $_GET['column'] : $columns[0];
    $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'desc' ? 'DESC' : 'ASC';
    $up_or_down = str_replace(array('ASC','DESC'), array('up','down'), $sort_order); 
    $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';
    $add_class = ' class="highlight"';
    if (isset($_GET['date_range'])) {
      //
    } else {
      echo "<i style='text-align:center;'>Please Select Date Range First.</i>";
    }
    ?>

    <table class="table align-items-center mb-0" id="myTable">
      <thead>
        <tr>
          <!-- <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2" onclick="#">No.</th> -->
          <th></th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-3">
            <?php
              if (isset($_GET['date_range'])) {
                echo  "<a href='reportpage_time.php?date_range=" . $date_range . 
                      "&column=date&order=" . $asc_or_desc . 
                      "&submit='>Working Date<i class='fas fa-sort $column == 'date' ? '-' . $up_or_down : '></i></a>
                      ";
              } else {
                echo "Working Date";
              }
            ?>
          </th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Employee Name</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Start Time</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">End Time</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Rate / Hour</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Total Time</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Total Paid</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">QTY</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">PC / Item</th>
          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Item / Hour</th>
        </tr>
      </thead>

    <?php 
    include 'conn.php'; //QUERY FOR SORT DATE FROM AND TO AND EMPLOYEE NAME
    //$no=1;

    $daterange = '';
    if(!empty($date_range)){
        $date_range = explode ("_", $date_range);

        $date_start = strtotime($date_range[0]);
        $mil = $date_start;
        $seconds = ceil($date_start / 1000);
        $date_start_conv = date("Y-m-d", $date_start);

        $date_end = strtotime($date_range[1]);
        $mil = $date_end;
        $seconds = ceil($date_end / 1000);
        $date_end_conv = date("Y-m-d", $date_end);
    }
    
    if(isset($_GET['submit'])){
      $columns = array('date');
      $column = isset($_GET['column']) && in_array($_GET['column'], $columns) ? $_GET['column'] : $columns[0];
      $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'desc' ? 'DESC' : 'ASC';
      $up_or_down = str_replace(array('ASC','DESC'), array('up','down'), $sort_order); 
      $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';
      $daterange = "WHERE daily_input.date BETWEEN '" . $date_start_conv . "' AND '" . $date_end_conv . "'";
      $sql = mysqli_query($koneksi,"
        -- SELECT 
        --  daily_input.date, 
        --  employee.first_name, 
        --  employee.last_name, 
        --  daily_input.start_time, 
        --  daily_input.endtime, 
        --  daily_input.total_time_in_sec, 
        --  employee.rate, 
        --  daily_input.total_paid, 
        --  (SELECT SUM(daily_input_detail.qty) GROUP BY daily_input_detail.id_daily_input) AS total_qty_daily,
        --  daily_input.total_packing_cost,
        --  daily_input.total_item_hour
        -- FROM daily_input
        -- INNER JOIN employee ON daily_input.id_employee = employee.id
        -- INNER JOIN daily_input_detail ON daily_input.id = daily_input_detail.id_daily_input
        -- $daterange
        -- GROUP BY daily_input.id 
        -- ORDER BY daily_input.id DESC
        SELECT
        daily_input.date,
        employee.first_name,
        employee.last_name,
        daily_input.start_time,
        daily_input.endtime,
        daily_input.total_time_in_sec,
        employee.rate,
        daily_input.total_paid,
        daily_input.id,
        daily_input.total_packing_cost,
        daily_input.total_item_hour
        FROM employee
        INNER JOIN daily_input ON employee.id = daily_input.id_employee
        $daterange
        ORDER BY daily_input.date " .$sort_order
      );
    } else {
      $sql = mysqli_query($koneksi,"
        -- SELECT 
        --  daily_input.date, 
        --  employee.first_name, 
        --  employee.last_name, 
        --  daily_input.start_time, 
        --  daily_input.endtime, 
        --  daily_input.total_time_in_sec, 
        --  employee.rate, 
        --  daily_input.total_paid, 
        --  (SELECT SUM(daily_input_detail.qty) GROUP BY daily_input_detail.id_daily_input) AS total_qty_daily,
        --  daily_input.total_packing_cost,
        --  daily_input.total_item_hour
        -- FROM daily_input
        -- INNER JOIN employee ON daily_input.id_employee = employee.id
        -- INNER JOIN daily_input_detail ON daily_input.id = daily_input_detail.id_daily_input
        -- GROUP BY daily_input.id
        -- ORDER BY daily_input.id DESC
        SELECT
        daily_input.date,
        employee.first_name,
        employee.last_name,
        daily_input.start_time,
        daily_input.endtime,
        daily_input.total_time_in_sec,
        employee.rate,
        daily_input.total_paid,
        daily_input.id,
        daily_input.total_packing_cost,
        daily_input.total_item_hour
        FROM employee
        INNER JOIN daily_input ON employee.id = daily_input.id_employee
        ORDER BY daily_input.date DESC;        
      ");
    }   

    while($d = mysqli_fetch_array($sql)){
      $id = $d['id'];
      $date = $d['date'];
      $employee_name = $d['last_name']." ".$d['first_name'];
      $start_time = $d['start_time'];
      $endtime = $d['endtime'];
      $rate = $d['rate'];
      $total_time_in_sec = $d['total_time_in_sec'];
      $total_paid = $d['total_paid'];
      $total_packing_cost = $d['total_packing_cost'];
      $total_item_hour = $d['total_item_hour'];      
    ?>

      <tbody>
        <tr>
          <td>
            <!-- <p class="text-xs font-weight-normal mb-0"><?php echo $no++; ?></p> -->
          </td>          
          <td>
            <div class="d-flex px-2">
              <div class="d-flex align-items-center">
                <h6 class="mb-0 text-xs"><?php $day = date('D', strtotime($date)); echo $day.", ".date('F d, Y', strtotime($date)); ?></h6>
              </div>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <p class="text-xs font-weight-normal mb-0 ps-3"><?php echo $employee_name; ?></p>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <?php echo $start_time; ?>
              </span>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-xs font-weight-normal mb-0 ps-1">
                <?php echo $endtime; ?>  
              </span>
            </div>
          </td>
          <td class="align-middle text-center">
            <div class="d-flex align-items-center">
              <span class="me-2 text-xs">
              <?php echo "$".$rate; ?>
              </span>
            </div>
          </td>          
          <td>
            <div class="d-flex align-items-center">
              <span class="text-xs font-weight-normal mb-0 ps-1">
              <?php 
              
                $init = $total_time_in_sec;
                // $hour = round(($init / 3600)); //hour
                // $minute = round(($init / 60) % 60); //minute                
                // if ($init > 60 AND $init < 3600) {
                //   echo $timeex = "0 H ".($minute % 60)." m";
                // } else if ($init > 3600 AND $init < 24*60*60) {
                //   echo $timeex = ($hour % 24)." H ".($minute % 60)." m";
                // }

                $hours = $init / 60;
                $mins = $hours % 60;
                $hours = $hours / 60;

                echo $timeex = ((int)$hours . " H " . (int)$mins . " m"); //ultimate time display

              ?>
              </span>
            </div>
          </td>
          <td class="align-middle text-center">
            <div class="d-flex align-items-center">
              <span class="me-2 text-xs">
              <?php 
              
                $amount = round(($init / 3600) * $rate, 2);
                echo "$".$amount;

              ?>
              </span>
            </div>
          </td>
          <td class="align-middle text-center">
            <div class="d-flex align-items-center">
              <span class="me-2 text-xs">
              <?php 
                //echo $d['total_qty_daily'];
                $qty_display = 0;
                $qty = 
                    "
                    SELECT SUM(daily_input_detail.qty) AS total_qty
                    FROM daily_input_detail
                    INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
                    WHERE daily_input_detail.id_daily_input = '".$id."' 
                    ";
                $queryqty = mysqli_query($koneksi, $qty);
                while ($qtyshow = mysqli_fetch_array($queryqty)) {
                  //$qty_display = $qtyshow['qty'];
                  $qty_display = $qtyshow['total_qty'];
                }

                if ($qty_display > 0) {
                  echo $qty_display;
                } else {
                  echo "0";
                }
              ?>
              </span>
            </div>
          </td>                              
          <td class="align-middle text-center">
            <div class="d-flex align-items-center">
              <span class="me-2 text-xs">
              <?php echo "$".$total_packing_cost; ?>
              </span>
            </div>
          </td>
          <td class="align-middle text-center">
            <div class="d-flex align-items-center">
              <span class="me-2 text-xs">
              <?php echo $total_item_hour; ?>
              </span>
            </div>
          </td>          
        </tr>
      </tbody>
      <?php
      }
      ?>
    </table>

    <!--TABEL ROW TO SEE THE TOTAL TIME AND AMOUNT PAYABLE BASED START AND END DATE AND BY EMPLOYEE-->
    <table class="table align-items-center mb-0" style="width:100%">
      <tr>
      <?php
      if(isset($_GET['submit'])){
        $sql = mysqli_query($koneksi,"
          SELECT 
              employee.last_name, 
              employee.first_name, 
              employee.rate, 
              daily_input.id_employee, 
              SUM(daily_input.total_time_in_sec) 
              AS Total_Time_In_Sec_Selected,
              SUM(daily_input.total_paid)
              AS Total_paid_selected,
              SUM(daily_input.total_packing_cost)
              AS Total_packing_cost_selected,
              SUM(daily_input.total_item_hour) 
              AS Total_item_hour_selected
          FROM employee 
          INNER JOIN daily_input
          ON employee.id = daily_input.id_employee 
          $daterange
          ");
        //***SQL diatas bisa menjumlahkan total waktu(dalam detik) berdasarkan pilihan waktu
      } else {
       $sql = mysqli_query($koneksi,"
          SELECT 
              employee.last_name, 
              employee.first_name, 
              employee.rate, 
              daily_input.id_employee, 
              SUM(daily_input.total_time_in_sec) 
              AS Total_Time_In_Sec_Selected,
              SUM(daily_input.total_paid)
              AS Total_paid_selected,
              SUM(daily_input.total_packing_cost)
              AS Total_packing_cost_selected,
              SUM(daily_input.total_item_hour) 
              AS Total_item_hour_selected
          FROM employee 
          INNER JOIN daily_input
          ON employee.id = daily_input.id_employee 
          ");
      }    

      while($d = mysqli_fetch_array($sql)){ 
      ?>
        <td style="width:57%" align="Center">
          <b>TOTAL</b>
        </td>
        <td style="width:10%">
          <b>
            <?php
           
            $total_time_from_query = $d['Total_Time_In_Sec_Selected'];
            $hour = round(($total_time_from_query / 3600)); //hour
            $minute = round(($total_time_from_query / 60) % 60); //minute                
            if ($total_time_from_query > 60 AND $total_time_from_query < 3600) {
              echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$timeex = "0 H ".($minute % 60)." m";
            } else if ($total_time_from_query > 3600 AND $total_time_from_query < 24*60*60) {
              echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$timeex = ($hour % 24)." H ".($minute % 60)." m";
            } else {
              echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$timeex = ($hour)." H ".($minute % 60)." m";
            }

            ?>
          </b>
        </td>
        <td style="width:9%">
          <b>
            <?php
                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$".$d['Total_paid_selected'];
            ?>
          </b>
        </td>
        <td style="width:6%">
          <b>
            <?php
            if(isset($_GET['submit'])){
              $sqlqty = mysqli_query($koneksi,"
                SELECT 
                  daily_input.date,
                  SUM(daily_input_detail.qty) AS Total_qty
                FROM daily_input_detail
                INNER JOIN daily_input ON daily_input_detail.id_daily_input = daily_input.id 
                $daterange
                ");
            } else {
              $sqlqty = mysqli_query($koneksi, "
                SELECT 
                 daily_input.id_employee, 
                 SUM(daily_input_detail.qty) AS Total_qty
                FROM employee 
                INNER JOIN daily_input ON employee.id = daily_input.id_employee 
                INNER JOIN daily_input_detail ON daily_input.id = daily_input_detail.id_daily_input
                WHERE daily_input.date
                ");
            }            
      
            while ($data = mysqli_fetch_array($sqlqty)) {
              echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data['Total_qty'];
            ?>
          </b>
        </td>        
        <td style="width:8%">
          <b>
            <?php
                //echo $d['Total_packing_cost_selected'];
                echo "&nbsp;&nbsp;&nbsp;&nbsp;".round($d['Total_paid_selected'] / $data['Total_qty'], 2);
            ?>
          </b>
        </td>
        <td style="width:10%">
          <b>
            <?php
                //echo $d['Total_item_hour_selected'];
                echo "&nbsp;&nbsp;&nbsp;".round($data['Total_qty'] / ($total_time_from_query / 3600), 2);            
            }            
            ?>
          </b>
        </td>        
      </tr>
      <?php
      }
      ?>
    </table>
  </div>
  </div>

</div>
<footer class="footer py-4  ">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="copyright text-center text-sm text-muted text-lg-start">
          © <script>
            document.write(new Date().getFullYear())
          </script>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link pe-0 text-muted" target="_blank">License</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>
</main>
    <div class="fixed-plugin">
    <a class="fixed-plugin-button text-dark position-fixed px-3 py-2">
      <i class="material-icons py-2">settings</i>
    </a>
    <div class="card shadow-lg">
      <div class="card-header pb-0 pt-3">
        <div class="float-start">
          <h5 class="mt-3 mb-0">Material UI Configurator</h5>
          <p>See our dashboard options.</p>
        </div>
        <div class="float-end mt-4">
          <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <!-- End Toggle Button -->
      </div>
      <hr class="horizontal dark my-1">
      <div class="card-body pt-sm-3 pt-0">
        <!-- Sidebar Backgrounds -->
        <div>
          <h6 class="mb-0">Sidebar Colors</h6>
        </div>
        <a href="javascript:void(0)" class="switch-trigger background-color">
          <div class="badge-colors my-2 text-start">
            <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
          </div>
        </a>

        <!-- Sidenav Type -->
        
        <div class="mt-3">
          <h6 class="mb-0">Sidenav Type</h6>
          <p class="text-sm">Choose between 2 different sidenav types.</p>
        </div>

        <div class="d-flex">
          <button class="btn bg-gradient-dark px-3 mb-2 active" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
          <button class="btn bg-gradient-dark px-3 mb-2 ms-2" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
          <button class="btn bg-gradient-dark px-3 mb-2 ms-2" data-class="bg-white" onclick="sidebarType(this)">White</button>
        </div>

        <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>
    
        <!-- Navbar Fixed -->
        
        <div class="mt-3 d-flex">
          <h6 class="mb-0">Navbar Fixed</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)">
          </div>
        </div>
           
        <hr class="horizontal dark my-3">
        <div class="mt-2 d-flex">
          <h6 class="mb-0">Light / Dark</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)">
          </div>
        </div>
        <hr class="horizontal dark my-sm-4">
            
        <a class="btn bg-gradient-info w-100" href="https://www.creative-tim.com/product/material-dashboard-pro">Free Download</a>
            
        <a class="btn btn-outline-dark w-100" href="https://www.creative-tim.com/learning-lab/bootstrap/overview/material-dashboard">View documentation</a>
        
        <div class="w-100 text-center">
          <a class="github-button" href="https://github.com/creativetimofficial/material-dashboard" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star creativetimofficial/material-dashboard on GitHub">Star</a>
          <h6 class="mt-3">Thank you for sharing!</h6>
          
          <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
          </a>
              
          <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
          </a>
          
        </div>
      </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../assets/js/core/popper.min.js" ></script>
<script src="../assets/js/core/bootstrap.min.js" ></script>
<script src="../assets/js/plugins/perfect-scrollbar.min.js" ></script>
<script src="../assets/js/plugins/smooth-scrollbar.min.js" ></script>

<script>
  var win = navigator.platform.indexOf('Win') > -1;
  if (win && document.querySelector('#sidenav-scrollbar')) {
    var options = {
      damping: '0.5'
    }
    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
  }
</script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=3.0.4"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
$(function() {

    <?php
    if(isset($_GET['date_range'])){
      $date_range = explode ("_", $_GET['date_range']); 
    ?>
      var start = moment(new Date('<?php echo $date_range[0]; ?>'));
      // var start = moment();//moment().subtract(29, 'days');
      var end = moment(new Date('<?php echo $date_range[1]; ?>'));
      // var end = moment();

      // var start = moment(new Date('<?php echo $date_start_conv; ?>'));
      // // var start = moment();//moment().subtract(29, 'days');
      // var end = moment(new Date('<?php echo $date_end_conv; ?>'));
      // // var end = moment();

    <?php
    } else {
    ?>
      var start = moment();//moment().subtract(29, 'days');
      var end = moment();
    <?php
    }
    ?>

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $("#date_range").val(start.format('YYYY-M-D') + '_' + end.format('YYYY-M-D'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Last 3 Months': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')],
           'Last 6 Months': [moment().subtract(5, 'month').startOf('month'), moment().endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
           'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

    cb(start, end);

});
</script>
</body>
</html>