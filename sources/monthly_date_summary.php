<?php 

if(!isset($_SESSION)) 
{ 
    session_start(); 
}

  //session_start();

  // do check
  if (!isset($_SESSION["username"])) {
      header("location: ../login.php");
      exit; // prevent further execution, should there be more code that follows
  }

  include 'conn.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <title>
    Prepcenter
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>  
</head>

<body class="g-sidenav-show  bg-gray-200">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="index.php">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Prepcenter</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white" href="index.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li>
          <a class="nav-link text-white" href="daily_input.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">add_task</i>
            </div>
            <span class="nav-link-text ms-1">Daily Input</span>
          </a>          
        </li>
        <?php
          if ($_SESSION['role_id'] == '1'){
              echo "
              <li class='nav-item'>
                <a class='nav-link text-white' href='employee.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>table_view</i>
                  </div>
                  <span class='nav-link-text ms-1'>Employee</span>
                </a>
              </li> 
              <li class='nav-item'>
                <a class='nav-link text-white' href='products.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>inventory</i>
                  </div>
                  <span class='nav-link-text ms-1'>Products</span>
                </a>
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Report pages</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Employee</span>
                </a> 
              </li>
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage_time.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Time</span>
                </a> 
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Summary</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white active bg-gradient-info' href='monthly_date_summary.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>app_registration</i>
                  </div>
                  <span class='nav-link-text ms-1'>Mothly Date Summary</span>
                </a> 
              </li>              
              ";
          } else {
              echo "";
          }
        ?>
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-info mt-4 w-100" href="logout.php" type="button">Log Out</a>
      </div>
    </div>    
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" data-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Monthly Date Summary</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Monthly Date Summary</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="#" class="nav-link text-body font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">
                  <?php echo $_SESSION['username']; ?>
                </span>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-right-from-bracket cursor-pointer"></i>
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="logout.php">
                    <div class="d-flex py-1">
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span>Log Out Now</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          <div id="current_date"></p>
                          <script>
                          document.getElementById("current_date").innerHTML = Date();
                          </script>                          
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->
<div class="container-fluid py-4">

  <div class="row">
    <div class="col-12">
      <div class="card my-4">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
          <div class="bg-gradient-info shadow-info border-radius-lg pt-2 pb-1">
            <div>
              <form method="get">
                <h6 class="text-white text-capitalize ps-3" align="left">Monthly Date Summary | 
                  <select name="id" class="my-3 pb-0" id="id">
                    <option selected disabled>- Select Employee -</option>
                    <?php
                    $sql = mysqli_query($koneksi, "SELECT * FROM employee WHERE employee_status = '11' ");
                    while ($d = mysqli_fetch_array($sql)) {
                    ?>
                    <option value="<?php echo $d['id']; ?>"><?php echo "VA".$d['id']." | ".$d['first_name']." ".$d['last_name']; ?></option>
                    <?php
                    }
                    ?>
                  </select>
                    <script type="text/javascript">
                      document.getElementById('id').value = "<?php echo $_GET['id'];?>";
                    </script>
                   | 
                  <input type="month" name="month" class="my-3 pb-0" id="month">
                    <script type="text/javascript">
                      document.getElementById('month').value = "<?php echo $_GET['month'];?>";
                    </script>
                  <button type="submit" name="submit" class="btn-success my-3 pb-0">Submit</button>
                  <button type="reset" value="Reset" class="btn-danger my-3 pb-0">Reset</button>
                </h6>
              </form>
            </div>
          </div>
        </div>
      <div class="card">
        <div class="table-responsive">
          <!--<br>
          <div class="alert alert-success alert-dismissible" id="success" style="display:none;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
          </div>-->

          <?php
          $columns = array('date');
          $column = isset($_GET['column']) && in_array($_GET['column'], $columns) ? $_GET['column'] : $columns[0];
          $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'desc' ? 'DESC' : 'ASC';
          $up_or_down = str_replace(array('ASC','DESC'), array('up','down'), $sort_order); 
          $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';
          $add_class = ' class="highlight"';

          if (isset($_GET['id'])) {
            $id = $_GET['id'];
          } else {
            $id = '';
          }

          if (isset($_GET['month'])) {
            $month = $_GET['month'];
          } else {
            $month = '';
          }

          if (isset($_GET['id'])) {
            //
          } else {
            echo "<i style='text-align:center;'>Please Select Employee and Month First.</i>";
          }

          ?>

          <table class="table align-items-center mb-0">
            <thead>
              <tr>
                <!-- <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ID</th> -->
                <th></th>
                <!-- <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Working Date</th> -->
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"><a href="monthly_date_summary.php?id=<?php echo $id; ?>&month=<?php echo $month; ?>&column=date&order=<?php echo $asc_or_desc; ?>&submit=">Working Date<i class="fas fa-sort<?php echo $column == 'date' ? '-' . $up_or_down : ''; ?>"></i></a></th>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Start Time</th>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">End Time</th>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Total Time</th>
                <th class="text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Update</th>
              </tr>
            </thead>
            <!-- <tbody id="table"> //content from view_ajax.php -->
            <tbody>
            <?php
            if(isset($_GET['submit'])){
              $columns = array('date');
              $column = isset($_GET['column']) && in_array($_GET['column'], $columns) ? $_GET['column'] : $columns[0];
              $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'desc' ? 'DESC' : 'ASC';
              $up_or_down = str_replace(array('ASC','DESC'), array('up','down'), $sort_order); 
              $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';
              $add_class = ' class="highlight"';              
              $employee_id = $_GET['id'];
              $month = $_GET['month'];
              $sql = "SELECT * FROM daily_input WHERE id_employee = '".$employee_id."' AND date LIKE '%".$month."%' ORDER BY date " .$sort_order;
              $result = $koneksi->query($sql);
            } else {
              $sql = "SELECT * FROM daily_input WHERE daily_input.date > NOW() - INTERVAL 60 DAY AND daily_input.date < NOW() + INTERVAL 60 DAY ORDER BY daily_input.date DESC";
              $result = $koneksi->query($sql);    
            }
            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) {
            ?>  
              <tr>
                <!-- <td>
                  <p class="text-s font-weight-normal mb-0 ps-4 p-0">
                    <?=$row['id'];?>
                  </p>
                </td> -->
                <td>
                  <p class="text-s font-weight-normal mb-0 p-0">
                  </p>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <p class="text-s font-weight-normal mb-0 ps-1 p-0">
                      <?php $date = $row['date']; $day = date('D', strtotime($date)); echo $day.", ".date('F d, Y', strtotime($date)); ?>
                    </p>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="text-dark text-xs">
                      <p class="text-s font-weight-normal mb-0 ps-2"><?=$row['start_time'];?></p>
                    </span>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="text-dark text-xs">
                      <p class="text-s font-weight-normal mb-0 ps-1"><?=$row['endtime'];?></p>
                    </span>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="text-dark text-xs">
                      <p class="text-s font-weight-normal mb-0 ps-1 p-0">
                        <?php
                          $time   = $row['total_time_in_sec'];
                                    $hours  = $time / 60;
                                    $mins   = $hours % 60;
                                    $hours  = $hours / 60;

                                    echo $timeex = ((int)$hours . " H " . (int)$mins . " m"); //ultimate time display
                        ?>

                      </p>
                    </span>
                  </div>
                </td>
                <td>
                  <button type="button" class="btn btn-info btn-sm update my-0 ps-1" data-toggle="modal" data-keyboard="false" data-backdrop="static" data-target="#update_time" data-id="<?=$row['id'];?>"
                  data-date="<?=$row['date'];?>"
                  data-start_time="<?=$row['start_time'];?>"
                  data-endtime="<?=$row['endtime'];?>"
                  data-total_time_in_sec="<?=$row['total_time_in_sec'];?>"
                  ">Update Time
                  </button>
                </td>
              </tr>
            <?php 
              }
            }
            ?>
            </tbody>
          </table>
        </div>
      </div>

      <!-- Modal Update-->
      <div class="modal fade" id="update_time" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-gradient-info" style="padding:6px;">
            <!--<div class="modal-header" style="color:#fff;background-color: #e35f14;padding:6px;">
              --><h5 class="modal-title"><i class="fa fa-edit"></i> Update Time</h5>
            </div>
          <div class="modal-body">
            <!--1-->
            <div class="row">
              <div class="col-md-3">
                  <label>ID</label>
              </div>
              <div class="col-md-9">
                <input type="text" name="id_modal" id="id_modal" class="form-control-sm" disabled>
              </div>  
            </div>
              <!--2-->
            <div class="row">
              <div class="col-md-3">
                  <label>Date</label>
              </div>
              <div class="col-md-9">
                <input type="text" name="date_modal" id="date_modal" class="form-control-sm" disabled>
              </div>  
            </div>
              <!--3-->
            <div class="row">
              <div class="col-md-3">
                  <label>Start Time</label>
              </div>
              <div class="col-md-9">
                <input type="time" name="start_time_modal" id="start_time_modal" class="form-control-sm" required>
              </div>  
            </div>
            <!--4-->
            <div class="row">
              <div class="col-md-3">
                  <label>End Time</label>
              </div>
              <div class="col-md-9">
                <input type="time" name="endtime_modal" id="endtime_modal" class="form-control-sm" required>
              </div>  
            </div>
            <input type="hidden" name="id_modal" id="id_modal" class="form-control-sm">
          </div>
          <div class="modal-footer" style="padding-bottom:0px !important;text-align:center !important;">
            <p style="text-align:center;float:center;"><button type="submit" id="update_time_button" class="btn btn-default btn-sm bg-gradient-info">Save</button>
            <button type="button" class="btn btn-default btn-sm bg-gradient-danger" data-dismiss="modal">Close</button></p>
          </div>
          </div>
        </div>
      </div>
      <!-- Modal End-->

      </div> <!-- card my-4 -->
    </div> <!-- col-12 -->
  </div> <!-- row -->
</div> <!-- container-fluid py-4 -->
<div class="container-fluid py-4">
<footer class="footer py-4">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="copyright text-sm text-muted text-lg-start">
          © <script>
            document.write(new Date().getFullYear())
          </script>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link text-muted" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link pe-0 text-muted" target="_blank">License</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>
</div>
</main>
    <div class="fixed-plugin">
    <a class="fixed-plugin-button text-dark position-fixed px-3 py-2">
      <i class="material-icons py-2">settings</i>
    </a>
    <div class="card shadow-lg">
      <div class="card-header pb-0 pt-3">
        <div class="float-start">
          <h5 class="mt-3 mb-0">Material UI Configurator</h5>
          <p>See our dashboard options.</p>
        </div>
        <div class="float-end mt-4">
          <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <!-- End Toggle Button -->
      </div>
      <hr class="horizontal dark my-1">
      <div class="card-body pt-sm-3 pt-0">
        <!-- Sidebar Backgrounds -->
        <div>
          <h6 class="mb-0">Sidebar Colors</h6>
        </div>
        <a href="javascript:void(0)" class="switch-trigger background-color">
          <div class="badge-colors my-2 text-start">
            <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
          </div>
        </a>

        <!-- Sidenav Type -->
        
        <div class="mt-3">
          <h6 class="mb-0">Sidenav Type</h6>
          <p class="text-sm">Choose between 2 different sidenav types.</p>
        </div>

        <div class="d-flex">
          <button class="btn bg-gradient-dark px-3 mb-2 active" data-class="bg-gradient-dark" onclick="sidebarType(this)">Dark</button>
          <button class="btn bg-gradient-dark px-3 mb-2 ms-2" data-class="bg-transparent" onclick="sidebarType(this)">Transparent</button>
          <button class="btn bg-gradient-dark px-3 mb-2 ms-2" data-class="bg-white" onclick="sidebarType(this)">White</button>
        </div>

        <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>
    
        <!-- Navbar Fixed -->
        
        <div class="mt-3 d-flex">
          <h6 class="mb-0">Navbar Fixed</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)">
          </div>
        </div>
           
        <hr class="horizontal dark my-3">
        <div class="mt-2 d-flex">
          <h6 class="mb-0">Light / Dark</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)">
          </div>
        </div>
        <hr class="horizontal dark my-sm-4">
            
        <a class="btn bg-gradient-info w-100" href="https://www.creative-tim.com/product/material-dashboard-pro">Free Download</a>
            
        <a class="btn btn-outline-dark w-100" href="https://www.creative-tim.com/learning-lab/bootstrap/overview/material-dashboard">View documentation</a>
        
        <div class="w-100 text-center">
          <a class="github-button" href="https://github.com/creativetimofficial/material-dashboard" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star creativetimofficial/material-dashboard on GitHub">Star</a>
          <h6 class="mt-3">Thank you for sharing!</h6>
          
          <a href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
          </a>
              
          <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
          </a>
          
        </div>
      </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../assets/js/core/popper.min.js" ></script>
<script src="../assets/js/core/bootstrap.min.js" ></script>
<script src="../assets/js/plugins/perfect-scrollbar.min.js" ></script>
<script src="../assets/js/plugins/smooth-scrollbar.min.js" ></script>

<script>
  var win = navigator.platform.indexOf('Win') > -1;
  if (win && document.querySelector('#sidenav-scrollbar')) {
    var options = {
      damping: '0.5'
    }
    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
  }
</script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>


<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=3.0.4"></script>
<script src="../assets/js/plugins/chartjs.min.js"></script>

<!-- Modal update time -->
<script>
$(document).ready(function() {
  $.ajax({
    url: "phpajax/view_ajax.php",
    type: "POST",
    cache: false,
    success: function(dataResult){
      $('#table').html(dataResult); 
    }
  });
  $(function () {
    $('#update_time').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); /*Button that triggered the modal*/
      var id = button.data('id');
      var date = button.data('date');
      var start_time = button.data('start_time');
      var endtime = button.data('endtime');
      var total_time_in_sec = button.data('total_time_in_sec');
      var modal = $(this);
      modal.find('#id_modal').val(id);
      modal.find('#date_modal').val(date);
      modal.find('#start_time_modal').val(start_time);
      modal.find('#endtime_modal').val(endtime);
      modal.find('#total_time_in_sec_modal').val(total_time_in_sec);
    });
    });
  $(document).on("click", "#update_time_button", function() { 
    $.ajax({
      url: "phpajax/update_ajax.php",
      type: "POST",
      cache: false,
      data:{
        id: $('#id_modal').val(),
        date: $('#date_modal').val(),
        start_time: $('#start_time_modal').val(),
        endtime: $('#endtime_modal').val(),
        total_time_in_sec: $('#total_time_in_sec_modal').val(),
      },
      success: function(dataResult){
        var dataResult = JSON.parse(dataResult);
        if(dataResult.statusCode==200){
          $('#update_time').modal().hide();
          alert('Data updated successfully !');
          location.reload();          
        }
      }
    });
  }); 
});
</script>
<!-- End Modal update time -->
</body>
</html>