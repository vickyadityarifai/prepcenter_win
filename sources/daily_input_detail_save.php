<?php

if(!isset($_SESSION)) 
{ 
    session_start(); 
}
//session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: ../login.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

$id_daily_input = $_REQUEST['id'];
$date = $_REQUEST['date_production'];
$fnsku = substr($_REQUEST['fnsku'], 0,10);
$input_to_fnsku = $fnsku;
$pack = $_REQUEST['pack'];
$qtyprod = $_REQUEST['qty'];

// echo "ID : ".$id_daily_input."<br />
//    date : ".$date."<br />
//    fnsku : ".$input_to_fnsku."<br />
//    pack : ".$pack."<br />
//    qty input daily detail : ".$qtyprod."<br />";

$query1 = "INSERT INTO daily_input_detail VALUES(null,'$id_daily_input','$input_to_fnsku','$pack','$qtyprod')";
mysqli_set_charset($koneksi, "utf8");
$sql1 = mysqli_query($koneksi, $query1); 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//query2 = Get total paid by working hour, get total qty per day by date to count total_packing_cost / day AND get result from (total qty daily / total time in hour) and insert to database too.

$get_total_paid = mysqli_query($koneksi, "SELECT total_time_in_sec, total_paid FROM daily_input WHERE id = '$id_daily_input'");
while($a = mysqli_fetch_array($get_total_paid)){
    $total_paid_daily = $a['total_paid'];
    $total_time_in_sec = $a['total_time_in_sec'];
}

$sql = mysqli_query($koneksi, "
  SELECT SUM(daily_input_detail.qty) AS total_qty
  FROM daily_input_detail
  INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
  WHERE daily_input_detail.id_daily_input = '$id_daily_input' AND daily_input.date = '$date'
  ");

while($totalqtydaily = mysqli_fetch_array($sql)){
  //echo "qty total by date and ID daily_input = ".$totalqtydaily['total_qty'];
  $totalqtydailytocount = $totalqtydaily['total_qty'];
}

$to_int = (int) $total_paid_daily;
//echo "<br> total paid daily = ".$to_int."<br>";
$total_paid_to_insert = round($to_int/$totalqtydailytocount, 2);
//echo $total_paid_to_insert;

$total_item_hour = round($totalqtydailytocount / ($total_time_in_sec / 3600), 2);
//echo $total_item_hour;

$query2 = "UPDATE daily_input SET total_packing_cost = '$total_paid_to_insert', total_item_hour = '$total_item_hour' WHERE id = '$id_daily_input' ";
$sql2 = mysqli_query($koneksi, $query2);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//query4 = Add new fnsku/product from daily input detail form And then Update PCS and QTY to table import_result

$sql3 = mysqli_query($koneksi, "
  INSERT INTO import_result VALUES (null,'Temporary Product Name', '0000000000', '0000000000', '$input_to_fnsku', 0, 0)
  ");

$sqlgetpcsandqty = mysqli_query($koneksi, "
  SELECT * 
  FROM import_result s1
  WHERE  id=(SELECT MIN(s2.id)
    FROM import_result s2
    WHERE s1.fnsku = s2.fnsku)
  AND fnsku = '$input_to_fnsku'
  ");
while ($updatepcsandqty = mysqli_fetch_array($sqlgetpcsandqty)) {
  $pcsawal = $updatepcsandqty['pcs'];
  $qtyawal = $updatepcsandqty['qty'];

$pcsakhir = $pcsawal + $pack; 
// echo "PCS Awal = ". $pcsawal."<br>";
// echo "PCS Akhir = ". $pcsakhir."<br>";
//echo "FNSKU = ".$input_to_fnsku."<br>"; //close to not showing
//echo "QTY Awal = ".$qtyawal."<br>"; //close to not showing
$qtyakhir = $qtyawal + $qtyprod;
//echo "QTY Akhir = ".$qtyakhir."<br>"; //close to not showing
}

$query4 = "UPDATE `import_result` SET `pcs`='$pcsakhir', `qty`='$qtyakhir' WHERE fnsku = '$input_to_fnsku' ";
$sql4 = mysqli_query($koneksi, $query4);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if($sql1 && $sql2 && $sql3 && $sql4) {
    echo "<script type='text/javascript'>document.location.href = 'daily_input_detail.php?id=$id_daily_input?alert=success';</script>";
} else {
    echo "<script type='text/javascript'>document.location.href = 'daily_input_detail.php?id=$id_daily_input?alert=failed';</script>";
}

?>