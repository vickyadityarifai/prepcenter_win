My note :
Template documentation https://www.creative-tim.com/learning-lab/bootstrap/overview/material-dashboard
Other references :
-IMPORT CSV files : https://www.webslesson.info/2020/06/import-selected-csv-file-column-in-php-using-ajax-jquery.html
-http://159.89.237.242/CSVGUI/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-Finding duplicate records and Delete Duplicate Records https://www.mysqltutorial.org/mysql-delete-duplicate-rows/ (AFTER DO IMPORT CSV)

-Query to knowing title who have two title name "Temporary Item Name" and "REAL NAME" **************************************** (IMPORTANT)
	SELECT *
	FROM import_result
	RIGHT JOIN tmp_import_result ON import_result.fnsku = tmp_import_result.fnsku
	WHERE import_result.title = 'Temporary Product Name'
	ORDER BY import_result.id ASC;
	//Query above Showing rows 0 - 188 (189 total) this mean, prepcenter have 189 product with "Temporary Product Name"
------------------------------------------------------------------------------------------------------------------------------------------
	(Knowing the duplicate records) import_result and tmp_import_result
	SELECT 
	    fnsku, 
	    COUNT(fnsku)
	FROM
	    import_result
	GROUP BY fnsku
	HAVING COUNT(fnsku) > 1;
------------------------------------------------------------------------------------------------------------------------------------------
	(delete the duplicate record on import_result table (keep the oldest id))
	DELETE c1 FROM import_result c1
	INNER JOIN import_result c2 
	WHERE
	    c1.id > c2.id AND 
	    c1.fnsku = c2.fnsku;

	(delete the duplicate record on tmp_import_result table (keep the newest id))
	DELETE c1 FROM tmp_import_result c1
	INNER JOIN tmp_import_result c2 
	WHERE
	    c1.id < c2.id AND 
	    c1.fnsku = c2.fnsku;		
------------------------------------------------------------------------------------------------------------------------------------------
-UPDATE RECORD BASED ON IMPORT FEATURE INPUT. BEFORE THIS STEP, DO FINDING DUPLICATES AND DELETE THE RECORDS ON IMPORT RESULT TABLE (WHERE c1.id > c2.id), ON TMP IMPORT RESULT TABLE (WHERE c1.id < c2.id).
	UPDATE import_result i 
	INNER JOIN tmp_import_result t on i.fnsku = t.fnsku 
	SET i.title = t.title, i.msku = t.msku, i.asin = t.asin 
	WHERE i.title = 'Temporary Product Name';
------------------------------------------------------------------------------------------------------------------------------------------
-Query to get data without duplicate (OLD WAY, NOT USE AGAIN)
	SELECT *
	FROM import_result s1
	WHERE id=(
		SELECT MIN(s2.id)
		FROM import_result s2
		WHERE s1.fnsku = s2.fnsku)
	ORDER BY id ASC
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-EXPORT DATATABLE https://phppot.com/php/export-datatables-data-using-php-and-mysql/
-Dynamic readonly field based on value, 0 = fillable, 0 != readonly
https://linuxhint.com/add-remove-readonly-attribute-javascript/#:~:text=To%20add%20and%20remove%20a%20readonly%20attribute%20in%20JavaScript%2C%20apply,attribute%20from%20the%20specified%20element.
https://www.tutorialspoint.com/How-to-change-the-value-of-an-attribute-in-javascript#

0. Move button cancel and import to above (head table)

==================================================================================================================================

									NEVER IMPORT ANY DATABASE FROM OUTSIDE OF THE SERVER.
											DATABASE ON SERVER IS THE NEWEST.
					ALWAYS CHECK THE DOUBLE QUOTE ON EVERY FILE WHO YOU WANT TO UPLOAD VIA IMPORT FEATURE
							   --=ALWAYS CHECK ANY DUPLICATE DATA ON IMPORT_RESULT TABLE.==-
								   ALWAYS REVIEW 	.htaccess, 
								   					sources/conn.php,
								   					sources/csvmapping/import.php
								   					sources/exp_datatable/server.php
								   AFTER UPLOADING										
==================================================================================================================================

0. [IDLE] Always do backup database and website file. And send to prepbackup@znzinc.com
	-As an automatic procedure. How about we create some executable file, who can :
		a. run job to create archive zip from public_html prepcenter. (zip -r prepcenter_ddmmyyyy.zip public_html) then copy to PREPCENTER BACKUP FOLDER ON kshah FOLDER.
		b. run job dump mysql database. (mysqldump -u [username] –p[password] [database_name] > inventorydb_313835556_ddmmyyyy.sql) then copy to PREPCENTER BACKUP FOLDER ON kshah FOLDER.
		IF MAKE SENSE
		c. send the two last or newest file (database.sql and website.zip) directly to prepbackup@znzinc.com
		d. create smtp server?

1. [IDLE] -hidden .php who showing in the URL

2. [IDLE] Dashboard add feature to showing data
	-Product with the highest QTY (Title + How much the QTY) - Product with the highest PCS (Title + How much the PCS) - 
	-https://www.google.com/search?q=membuat+chart+dengan+chart+js&sca_esv=599390440&sxsrf=ACQVn0-E-0SBW-rioud7esRilVfpx4KgBA%3A1705565775032&source=hp&ei=Tt6oZZ6sPOX0juMPrtOUqAI&iflsig=ANes7DEAAAAAZajsX9i5_MVvpCKA2gADy_46zZEEQPMz&oq=membuat+chart+dengan+cha&gs_lp=Egdnd3Mtd2l6IhhtZW1idWF0IGNoYXJ0IGRlbmdhbiBjaGEqAggAMgUQABiABEiQKlAAWIIicAB4AJABAJgBaKABwQyqAQQyMy4xuAEDyAEA-AEBwgIKECMYgAQYigUYJ8ICBBAjGCfCAgoQABiABBiKBRhDwgILEAAYgAQYsQMYgwHCAgUQLhiABMICCxAuGIAEGMcBGNEDwgIIEAAYgAQYsQPCAggQABiABBjLAcICBhAAGBYYHsICCBAAGBYYHhgP&sclient=gws-wiz (I want to create CHARTs)
	-https://www.rajaputramedia.com/artikel/membuat-grafik-chart-js-dengan-php-mysql.php
	-https://www.malasngoding.com/membuat-grafik-dari-database-mysql-dan-php-dengan-chart-js/ ///////THIS IS THE TUTORIAL/////////
	-Charts is display, but the value of qty is not separated in array maybe we can try to get other tutorial charts js in the long link above
	-DONT FORGET TO MAKE THIS FEATURE JUST ONLY CAN SEEING BY ROLE = 1

3. [IDLE] On Daily Input Detail Page :
	Pack field is only available if the fnsku/product, does not have the pack or pack = 0, if pack is exist, pack field is readonly.
	--- Change Feature to autofill first.
	--- The feature to get pcs and qty is successfull, the next is to make requirement like point no 4 and the saving process.
	THIS FEATURE DEVELOPMENT IS STILL ON DAILY INPUT DETAIL PROTOTYPE MENU

4. Implementing database management, continously in number 3 on product_review page.

----------------------------------------------------------------------------------------------------------------------------------